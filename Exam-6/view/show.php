<?php
include_once '../vendor/autoload.php';
use packegApp\semistarPackeg\semistarPackeg;

$obj = new semistarPackeg();
$obj->prepare($_GET);
$singledata = $obj->singleDataShow();


if (isset($singledata) && !empty($singledata)) {
    ?>

    <table border="1">
        <tr>
            <th>ID</th>
            <th>Unique ID</th>
            <th>Name</th>
            <th>Semister</th>
            <th>offer</th>
            <th>Cost</th>
            <th>Weabar</th>
            <th>Total cost</th>
        </tr>
        <tr>
            <td><?php echo $singledata['id'] ?></td>
            <td><?php echo $singledata['unique_id'] ?></td>
            <td><?php echo $singledata['name'] ?></td>
            <td><?php echo $singledata['semister'] ?></td>
            <td><?php echo $singledata['offer'] ?></td>
            <td><?php echo $singledata['cost'] ?></td>
            <td><?php echo $singledata['webar'] ?></td>
            <td><?php echo $singledata['total_cost'] ?></td>
        </tr>
    </table>
    <?php
} else {
    $_SESSION['err_msg'] = "Not found. Something going wrong !" . "<a href='create.php'>Create</a>";
    header('location:error.php');
}
?>

<h1><a href="index.php">Back to List</a></h1>