<?php
include_once '../vendor/autoload.php';
use packegApp\semistarPackeg\semistarPackeg;

$obj = new semistarPackeg();
$obj->prepare($_GET);
$single=$obj->singleDataShow();
//echo'<pre>';
//print_r($single);

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1 style="text-align: center;">Update your info</h1>
        <a href="index.php">show list</a>
        <form action="update.php" method="post">
            <table>
                <tr>
                    <td>Name :</td>
                    <td><input type="text" name="name" value="<?php echo $single['name'] ?>" placeholder="Enter your name" />
                        <?php
                        if (isset($_SESSION['name_msg']) && !empty($_SESSION['name_msg'])) {
                            echo $_SESSION['name_msg'];
                            unset($_SESSION['name_msg']);
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Semister :</td>
                    <td><input type="text" name="semister" value="<?php echo $single['semister'] ?>" placeholder="Enter your semister" />
                        <?php
                        if (isset($_SESSION['semister_msg']) && !empty($_SESSION['semister_msg'])) {
                            echo $_SESSION['semister_msg'];
                            unset($_SESSION['semister_msg']);
                        } elseif(isset($_SESSION['condition_msg']) && !empty($_SESSION['condition_msg'])) {
                             echo $_SESSION['condition_msg'];
                            unset($_SESSION['condition_msg']);
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="checkbox" name="offer" value="yes" /> Get Offer</td>
                    <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Submit" /></td>
                </tr>
            </table>

        </form>

        <style type="text/css">
            input[type=text]{border:1px solid #eee; height: 25px; padding: 4px 10px;}
        </style>
    </body>
</html>
