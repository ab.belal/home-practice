<?php

namespace App\Users;

use PDO;

class Users
{
    public $id = '';
    public $uid = '';
    public $vid = '';
    public $dashid = '';
    public $title = '';
    public $password = '';
    public $repassword = '';
    public $email = '';
    public $data = '';

    public $logemail = '';
    public $logpassword = '';

    public $con = '';

    public function __construct()
    {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
//        $this->con = new PDO('mysql:host=localhost;dbname=themeyellow_php-26-arafat', "themeyellow_WNbdTy", "1u!c1wp(CXAI");
        $this->con = new PDO('mysql:host=localhost;dbname=php-26-arafat', "root", "");
    }

    public function prepare($data = '')
    {
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['usname'])) {
            $this->title = $data['usname'];
        }

        if (!empty($data['password'])) {
            $this->password = $data['password'];
        }

        if (!empty($data['re-password'])) {
            $this->repassword = $data['re-password'];
        }

        if (!empty($data['email'])) {
            $this->email = $data['email'];
        }

        if (!empty($data['logemail'])) {
            $this->logemail = $data['logemail'];
        }

        if (!empty($data['logpassword'])) {
            $this->logpassword = $data['logpassword'];
        }
    }

    public function Validation($vlim = "")
    {
        if (isset($_SESSION["$vlim"]) && !empty($_SESSION["$vlim"])) {
            echo $_SESSION["$vlim"];
            unset($_SESSION["$vlim"]);
        }
    }

    public function Verification()
    {
        try {
            // UserName
            if (!empty($this->title)) {
                if (strlen($this->title) >= 6 && strlen($this->title) <= 12) {
                    $query = "SELECT * FROM reg_form WHERE user_name= '$this->title'";
                    $query = $this->con->prepare($query);
                    $query->execute();
                    $row = $query->fetch();
                    if (!empty($row['user_name'])) {
                        $_SESSION['US_N'] = "This user name already used, try another one.";
                    }
                } else {
                    $_SESSION['US_N'] = "User name Must be 6 and 12 letter *";
                }
            } else {
                $_SESSION['US_N'] = "User name required *";
            }

            // Password
            if (!empty($this->password)) {
                if (strlen($this->password) >= 6 && strlen($this->password) <= 12) {
                    if ($this->password !== $this->repassword) {
                        $_SESSION['US_PM'] = "Password doesn't match, Please try again *";
                    }
                    /*else {

                    }*/
                } else {
                    $_SESSION['US_P'] = "Password Must be 6 and 12 letter *";
                }
            } else {
                $_SESSION['US_P'] = "Password required *";
            }

            // Email
            if (!empty($this->email)) {
                if (!filter_var($this->email, FILTER_VALIDATE_EMAIL) === false) {
                    $query = "SELECT * FROM reg_form WHERE email = '$this->email'";
                    $query = $this->con->prepare($query);
                    $query->execute();
                    $row = $query->fetch();

                    if (!empty($row['email'])) {
                        $_SESSION['U_EM'] = "This email already used, try another one.";
                    }
                    /*else {
                        $this->email = $data['email'];
                    }*/

                } else {
                    $_SESSION['U_EM'] = "Email address is not valid *";
                }
            } else {
                $_SESSION['U_EM'] = "Email required *";
            }

            // LogEmail
            if (!empty($this->logemail)) {
                $_SESSION['Log_EV'] = $this->logemail;
            } else {
                $_SESSION['Log_EM'] = "Email required *";
            }
            // Logpass
            if (!empty($this->logpassword)) {
                $_SESSION['Log_PV'] = $this->logpassword;
            } else {
                $_SESSION['Log_P'] = "Password required *";
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function store()
    {
        if (!empty($this->title) && !empty($this->password) && !empty($this->email)) {
            try {

                $this->uid = uniqid();
                $this->vid = uniqid();

                $qr = "INSERT INTO reg_form(id, unique_id, verification_id, user_name, password, email, is_active, is_admin, is_delete, created)
    VALUES(:id, :uniqueid, :vid, :usrnm, :pw, :eml, :isa, :iad, :isd, :crtd)";

                $statement = $this->con->prepare($qr);
                $statement->execute(array(
                    ':id' => null,
                    ':uniqueid' => $this->uid,
                    ':vid' => $this->vid,
                    ':usrnm' => $this->title,
                    ':pw' => $this->password,
                    ':eml' => $this->email,
                    ':isa' => 0,
                    ':iad' => 0,
                    ':isd' => 0,
                    ':crtd' => date("Y-m-d h:i:s")
                ));
                $_SESSION['R_messages'] = "Successfully Sign Up, Checkout your email for Verification";

                $to = $this->email;
                $subject = 'Signup | Verification';
                $message = '
                    Thanks for signing up!
                    Activated your account by pressing the url below.

                    ------------------------
                    Username: ' . $this->title . '
                    Password: ' . $this->password . '
                    ------------------------

                    Please click this link to activate your account:
                    http://www.themeyellow.com/testing/crud/views/verify.php?vid=' . $this->vid . '';

                $headers = 'From:noreply@themeyellow.com' . "\r\n";
                echo $message;

//                mail($to, $subject, $message, $headers);

//                header("location:login.php");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }

        } else {
            $_SESSION['N_va'] = $this->title;
            $_SESSION['P_va'] = $this->password;
            $_SESSION['RP_va'] = $this->repassword;
            $_SESSION['E_va'] = $this->email;
            header("location:signup.php");
        }
    }


    /*public function verify()
    {
        try {
            $qr = "SELECT * FROM reg_form WHERE verification_id='" . $_GET['vid'] . "'";
            $query = $this->con->prepare($qr);
            $query->execute();
            $row = $query->fetch();
            if (empty($row['verification_id'])) {
                $_SESSION['Errors_R'] = 'Registration Error.';
                header("location:errors.php");
            }
            return $row;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }*/

    public function VFiy()
    {
        try {
            $qr = "SELECT * FROM reg_form WHERE verification_id='" . $_GET['vid'] . "'";
            $query = $this->con->prepare($qr);
            $query->execute();
            $row = $query->fetch();

            if ($row['is_active'] == 1) {
                $_SESSION['R_C'] = "Email Already Verified.";
                header("location:login.php");
            } else {
                try {
//                $qr = "UPDATE  themeyellow_php-26-arafat.reg_form SET  is_active =  '1' WHERE  reg_form.verification_id ='" . $_GET['vid'] . "'";
                    $qr = "UPDATE reg_form SET is_active = '1' WHERE reg_form.verification_id ='" . $_GET['vid'] . "'";
                    $query = $this->con->prepare($qr);
                    $query->execute();
                    $_SESSION['confirm'] = "Registration Process Completed. Now you can Login";
                    header("location:login.php");
                } catch (PDOException $e) {
                    echo 'Error: ' . $e->getMessage();
                }
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function login()
    {
        try {
            $query = "SELECT * FROM reg_form WHERE email = '$this->logemail' AND password = '$this->logpassword'";
            $query = $this->con->prepare($query);
            $query->execute();
            $row = $query->fetch();

            if (isset($row) && !empty($row)) {
                if ($row['is_delete'] == 0){
                    if ($row['is_active'] == 1) {
                        $_SESSION['Login_data'] = $row;
                        $_SESSION['Login'] = "Successfully Login";
                        header("location:dashbord.php");
                    } else {
                        $_SESSION['E_active'] = "Your account not verified yet. Check your email and verify";
                        header("location:login.php");
                    }
                } else {
                    $_SESSION['Is_D'] = "Your account was deleted, Now you can SignUp with new email.";
                    header("location:signup.php");
                }

            } else {
                if ($row['email'] == $this->logemail && $row['password'] == $this->logpassword) {
                    header("location:login.php");
                } else {
                    $_SESSION['EP_M'] = "Email & Password dose not matched.";
                    header("location:login.php");
                }
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function logout()
    {
        try {
            $_SESSION['Logout_M'] = "Successfully logout";
            unset($_SESSION['Login_data']);
            header("location:login.php");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function index()
    {
        try {
            $qr = "SELECT * FROM reg_form WHERE is_delete = 0";
            $query = $this->con->prepare($qr);
            $query->execute();

            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
            return $this->data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function show()
    {
        try {
            $qr = "SELECT * FROM reg_form WHERE unique_id=" . "'" . $this->id . "'";
            $query = $this->con->prepare($qr);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);
            return $row;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function update()
    {
        try {
            $query = "UPDATE reg_form SET user_name = :un, email = :e, password = :p, modified= :modi WHERE reg_form.unique_id = :uid";
            $stmt = $this->con->prepare($query);
            $stmt->execute(array(
                ':un' => $this->title,
                ':e' => $this->email,
                ':p' => $this->password,
                ':modi' => date("Y-m-d h:i:s"),
                ':uid' => $this->id,
            ));
            $_SESSION['Up_M'] = "Data Updated";
            header("location:pro-update.php?id=$this->id");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function trash()
    {
        try {

            $query = "UPDATE reg_form SET is_delete = :isdele, deleted = :deletime WHERE reg_form.unique_id = :uid";
            $stmt = $this->con->prepare($query);
            $stmt->execute(array(
                ':isdele' => 1,
                ':deletime' => date("Y-m-d h:i:s"),
                ':uid' => $this->id,
            ));
            $_SESSION['Tr_D'] = "Data Successfully deleted";
            header("location:all-data.php");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function reStore()
    {
        try {

            $query = "UPDATE reg_form SET is_delete = :isdele, restore = :restime WHERE reg_form.unique_id = :uid";
            $stmt = $this->con->prepare($query);
            $stmt->execute(array(
                ':isdele' => 0,
                ':restime' => date("Y-m-d h:i:s"),
                ':uid' => $this->id,
            ));
            $_SESSION['Re_S'] = "Data Successfully ReStored";
            header("location:all-data.php");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function trashItems()
    {
        try {
            $qr = "SELECT * FROM reg_form WHERE is_delete = 1";
            $query = $this->con->prepare($qr);
            $query->execute();

            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
            return $this->data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

    public function delete()
    {
        try {
            $query = "DELETE FROM reg_form WHERE unique_id = :uid";
            $stmt = $this->con->prepare($query);
            $stmt->bindParam(':uid', $this->id);
            $stmt->execute();
            $_SESSION['I_D_P'] = "Item Deleted permanently.";
            header("location:trash-item.php");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }


}
