<?php
include_once "../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();

$alldata = $obj->trashItems();

if (isset($_SESSION['Login_data']) && !empty($_SESSION['Login_data'])) {
    ?>

    <html>
    <head>
        <title>Index | Page</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
    <?php include_once "include/log-navbar.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-top: 50px">
                <h3 class="text-center">User List</h3>
                <strong class="text-success"><?php $obj->Validation("I_D_P") ?></strong>

                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>ID</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Is Active</th>
                        <th>Is Admin</th>
                        <th>Is delete</th>
                        <th>Created</th>
                        <th>Modified</th>
                        <th>Deleted</th>
                        <th>ReStore</th>
                        <th colspan="2">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $serial = 1;
                    if (isset($alldata) && !empty($alldata)) {
                        foreach ($alldata as $onedata) {
                            ?>
                            <tr>
                                <td><?php echo $serial++; ?></td>
                                <td><?php echo $onedata['id'] ?></td>
                                <td><?php echo $onedata['user_name'] ?></td>
                                <td><?php echo $onedata['email'] ?></td>
                                <td><?php echo $onedata['password'] ?></td>
                                <td><?php echo $onedata['is_active'] ?></td>
                                <td><?php echo $onedata['is_admin'] ?></td>
                                <td><?php echo $onedata['is_delete'] ?></td>
                                <td><?php echo $onedata['created'] ?></td>
                                <td><?php echo $onedata['modified'] ?></td>
                                <td><?php echo $onedata['deleted'] ?></td>
                                <td><?php echo $onedata['restore'] ?></td>
                                <td><a href="restore.php?id=<?php echo $onedata['unique_id'] ?>">ReStore</a></td>
                                <td><a href="delete.php?id=<?php echo $onedata['unique_id'] ?>">Delete</a></td>

                            </tr>
                        <?php }
                    } else { ?>
                        <tr>
                            <td colspan="12">No available data</td>
                        </tr>

                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </body>
    </html>
    <?php
} else {
    $_SESSION['Errors_R'] = "User not found :(";
    header("location:errors.php");
}
?>
