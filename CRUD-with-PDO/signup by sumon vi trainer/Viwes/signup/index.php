<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd"
    >
<html lang="en">
<head>
    <title>Register form with HTML5 using placeholder and CSS3</title>
</head>
<style type="text/css">
  
</style>
<link rel="stylesheet" type="text/css" href="style.css">
<body>
    <div id="wrapper">
        <form action="store.php" method="post">
            <fieldset>
                <legend>Register Form</legend>
                <div>
                    <input type="text" name="user_name" placeholder="username"/>
                </div>

                <div>
                    <input type="password" name="pass" placeholder="Password"/>
                </div>
                <div>
                    <input type="password" name="repass" placeholder="Re-Password"/>
                </div>
                <div>
                    <input type="text" name="email" placeholder="Email"/>
                </div>
  
                <input type="submit" name="submit" value="Send"/>
            </fieldset>    
        </form>
    </div>
</body>
</html>
