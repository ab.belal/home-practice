<?php

namespace mobileApp;

use PDO;

class Mobilemodel {

    public $id = '';
    public $model = '';
    public $laptop = '';
    public $mobileDataList = '';
    public $dbuser = 'root';
    public $dbpass = '';
    public $conn = '';

    public function __construct() {
        session_start();
        $this->conn = new PDO("mysql:host=localhost;dbname=personalinfo", $this->dbuser, $this->dbpass);
    }

    public function dataPassToProperty($data='') {
        if (array_key_exists('mModel', $data) && !empty($data['mModel'])) {
            $this->model = $data['mModel'];
        } else {
            $_SESSION['mblemty'] = "mobile model required";
        }

        if (array_key_exists('lModel', $data) && !empty($data['lModel'])) {
            $this->laptop = $data['lModel'];
        } else {
            $_SESSION['ltpemty'] = "Laptop model required";
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        $_SESSION['formData'] = $data;
        return $this;
    }

    public function store() {
        try {
            $insertQuery = "INSERT INTO mobilemodels(id,models,laptop_model,unique_id)VALUES(:id, :model, :laptop_model, :unique_id)";
            $stmt = $this->conn->prepare($insertQuery);
            $stmt->execute(array(
                ':id' => null,
                ':model' => $this->model,
                ':laptop_model' => $this->laptop,
                ':unique_id' => uniqid(),
            ));
            //print_r($insertQuery);
            //die();
            header('location:index.php');
        } catch (PDOException $err) {
            echo'error' . $err->getMessage();
        }
    }

    public function mobileList() {
        $listGetQuery = "SELECT * FROM `mobilemodels`";
        $queryData = $this->conn->prepare($listGetQuery);
        $queryData->execute();
        while ($singleQueryData = $queryData->fetch(PDO::FETCH_ASSOC)) {
            $this->mobileDataList[] = $singleQueryData;
        }
        return $this->mobileDataList;
    }

    public function singleDataShow() {
        $showQuery = "SELECT * FROM `mobilemodels` WHERE `unique_id` =" . "'$this->id'" . "";
        $mydata = $this->conn->prepare($showQuery);
        $mydata->execute();
        $row = $mydata->fetch(PDO::FETCH_ASSOC);
        return $row;


    }

    public function dataUpdate() {
        try {
            $updateQuery = "UPDATE `mobilemodels` SET `models`=:molels WHERE `unique_id`=:unique_id";
            $stmt = $this->conn->prepare($updateQuery);
            $stmt->execute(array(
                ':molels' => $this->model,
                ':unique_id' => $this->id,
            ));
            $_SESSION['message'] = "successfully updated";
            header("location:edit.php?id=$this->id");
            //echo $stmt->rowCount();
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function dataDelete() {
        try {
            $deleteQuery = "DELETE FROM `mobilemodels` WHERE `mobilemodels`.`unique_id` = :uid";
            $stmt = $this->conn->prepare($deleteQuery);
            $stmt->bindParam(':uid', $this->id); // this time, we'll use the bindParam method
            $stmt->execute();

            $_SESSION['message'] = "<h1>data deleted have you any problem..?</h1>";
            header("location:index.php");
            //echo $stmt->rowCount(); // 1
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

}

?>
