<?php

namespace RegistrationApp\Bitm\user_registration;

use PDO;

class UserRegistration {

    public $id, $uid, $vid, $data, $uname, $upass, $upass_confrm, $email, $error,
            $is_active, $is_admin, $created, $deleted, $modified, $conn, $dbuser = 'root', $dbpass = '';

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=tproject', $this->dbuser, $this->dbpass);
    }

    public function prepare($data = '') {
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['username'])) {
            $this->uname = $data['username'];
        }
        if (!empty($data['password'])) {
            $this->upass = $data['password'];
        }
        if (!empty($data['password-retype'])) {
            $this->upass_confrm = $data['password-retype'];
        }
        if (!empty($data['email'])) {
            $this->email = $data['email'];
        }
        if (!empty($data['uid'])) {
            $this->uid = $data['uid'];
        }
    }

    public function sessionMessage($val_sess = '') {
        if (!empty($_SESSION["$val_sess"]) && isset($_SESSION["$val_sess"])) {
            echo $_SESSION["$val_sess"];
            unset($_SESSION["$val_sess"]);
        }
    }

    function confirm() {
        if (!empty($this->uname)) {
            if (strlen($this->uname) >= 6 && strlen($this->uname) <= 12) {
                $unikeid = $this->conn->query("SELECT `username` FROM `users` WHERE `username`= '" . $this->uname . "' ");
                $row = $unikeid->fetch();
                if (!empty($row['username'])) {
                    $_SESSION['val_username_unike'] = $this->uname . ' Already Exist. Please Try Another One.<br/>';
                    $this->error = true;
                }  else {
                    $_SESSION['valu_username'] = $this->uname;
                }
            } else {
                $_SESSION['val_username_lenth'] = 'Username Must Be Within 6 to 12 Character.<br/>';
                $this->error = true;
            }
        } else {
            $_SESSION['val_username_req'] = 'Username Required.<br/>';
            $this->error = true;
        }

        if (!empty($this->upass)) {
            if (strlen($this->upass) >= 6 && strlen($this->upass) <= 12) {
                
            } else {
                $_SESSION['val_pass_lenth'] = 'Password Must Be Within 6 to 12 Character.<br/>';
                $this->error = true;
            }
        } else {                  
            $_SESSION['val_pass_req'] = 'Enter Password.<br/>';
            $this->error = true;
        }

        if (!empty($this->upass_confrm)) {
            if ($this->upass !== $this->upass_confrm) {
                $_SESSION['val_pass_match'] = 'Password does not Matched.<br/>';
                $this->error = true;
            }
        } else {
            $_SESSION['val_pass_retype'] = 'Retype Password.<br/>';
            $this->error = true;
        }

        if (!empty($this->email)) {
            if (filter_var($this->email, FILTER_VALIDATE_EMAIL) == TRUE) {
                $unike_email = $this->conn->query("SELECT `email` FROM `users` WHERE `email`= '" . $this->email . "' ");
                $row = $unike_email->fetch();
                if (!empty($row['email'])) {
                    $_SESSION['val_email_unike'] = $this->email . ' Already Used. Please Try Another Email.<br/>';
                    $this->error = true;
                } else {
                    $_SESSION['valu_email'] = $this->email;
                }
            } else {
                $_SESSION['val_email_valid'] = 'Email Address is not valid.<br/>';
                $this->error = true;
            }
        } else {
            $_SESSION['val_email_req'] = 'Email Address Required.<br/>';
            $this->error = true;
        }
    }

    public function store() {
        if ($this->error == FALSE) {
            try {
                $this->uid = uniqid();
                $this->vid = uniqid();
                $query = "INSERT INTO `tproject`.`users` (`id`, `uid`, `vid`, `username`, `password`, `email`, `is_active`, `is_admin`, `created`, `modified`, `deleted`) VALUES (:id ,:uid,:vid,:uname,:upass,:mail,:iactive,:iadmin,:created,:modified,:deleted)";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':id' => null,
                    ':uid' => $this->uid,
                    ':vid' => $this->vid,
                    ':uname' => $this->uname,
                    ':upass' => $this->upass,
                    ':mail' => $this->email,
                    ':iactive' => '0', //$this->is_active, 
                    ':iadmin' => '0', //$this->is_admin, 
                    ':created' => date("Y-m-d h:i:s"),
                    ':modified' => '',
                    ':deleted' => '0', //$this->deleted
                ));
                $_SESSION['message'] = "Data Submitted Successfully.";
                header("location:register.php?vid=$this->vid");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header('location:index.php');
        }
    }

    public function active() {
        $vid = $this->conn->query("SELECT * FROM `users` WHERE `vid`= '" . $_GET['vid'] . "' ");
        $row = $vid->fetch();
        if (empty($row['vid'])) {
            $_SESSION['error'] = 'Registration Error.';
            header('location:error.php');
        } else {
            if ($row['is_active'] == 1) {
                $_SESSION['error'] = 'Email Already Verified.Go to <a href="login.php">login page</a>.';
                header('location:error.php');
            } else {
                try {
                    $query = $this->conn->prepare("UPDATE `users` SET `is_active` = '1' WHERE `users`.`vid` = '" . $_GET['vid'] . "'");
                    $query->execute();
                    $_SESSION['confirm'] = "Registration Process Compleated. Now Login.";
                    header('location:login.php');
                } catch (PDOException $e) {
                    
                }
            }
        }
    }
}
