<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
</head>
<body>
    <h3>Login</h3>
    <?php
    if (isset($_SESSION['confirm']) && !empty($_SESSION['confirm'])) {
        echo $_SESSION['confirm'];
        unset($_SESSION['confirm']);
    }
    ?>
    <form action="" method="post">
        <table>
            <tr>
                <td><label>Username :</label></td>
                <td><input type="text" name="name"/></td>
            </tr>
            <tr>
                <td><label>Password :</label></td>
                <td><input type="password" name="password"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Login"/></td>
            </tr>
        </table>
    </form>
</body>
</html>