<?php
include '../../../vendor/autoload.php';
use RegistrationApp\Bitm\user_registration\UserRegistration;
$obj = new UserRegistration();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>User Registration</title>
</head>
<body>
    <h3>Sign Up First</h3>
    <form method="post" action="store.php">
        <table>
            <tr>
                <td><label>Username : </label></td>
                <td><input type="text" name="username" value="<?php $obj->sessionMessage('valu_username');?>"/></td>
                <td><?php $obj->sessionMessage('val_username_unike');$obj->sessionMessage('val_username_lenth');$obj->sessionMessage('val_username_req');?></td>
            </tr>
            <tr>
                <td><label>Password :</label></td>
                <td><input type="password" name="password"/></td>
                <td><?php $obj->sessionMessage('val_pass_lenth');$obj->sessionMessage('val_pass_req');?></td>
            </tr>
            <tr>
                <td><label>Retype Password :</label></td>
                <td><input type="password" name="password-retype"/></td>
                <td><?php $obj->sessionMessage('val_pass_match');$obj->sessionMessage('val_pass_retype');?></td>
            </tr>
            <tr>
                <td><label>Email :</label></td>
                <td><input type="text" name="email" value="<?php $obj->sessionMessage('valu_email');?>"/></td>
                <td><?php $obj->sessionMessage('val_email_valid');$obj->sessionMessage('val_email_req');$obj->sessionMessage('val_email_unike');?></td>
            </tr>
            <tr>
                <td><input type="submit" value="Confirm"/> <input type="reset"/></td>
                <td></td>
            </tr>
        </table>
    </form>
</body>
</html>