<?php

namespace signupForm\resistrationForm;

use PDO;

class resistrationForm {

    //resistration form property
    public $id = '';
    public $verificationId = '';
    public $userName = '';
    public $email = '';
    public $password = '';
    public $repPassword = '';
    public $dbUser = 'root';
    public $dbPassword = '';
    public $dbConnect = '';
    public $isActive = '0';
    public $isAdmin = '0';
    public $created = '2016';
    public $modified = '10';
    public $delete = 'Feb';

    public function __construct() {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
        $this->dbConnect = new PDO("mysql:host=localhost;dbname=resistrationform", $this->dbUser, $this->dbPassword);
    }

    //prepare methode
    public function prepare($formAllData='') {
        //for id pass to property
        if (array_key_exists('id', $formAllData)) {
            $this->id = $formAllData['id'];
        }

        //for User Name pass to property
        if (array_key_exists('userName', $formAllData) && !empty($formAllData['userName'])) {
            $this->userName = $formAllData['userName'];
        } else {
            $_SESSION['errName'] = "Name required";
        }

        //for Email pass to property
        if (array_key_exists('email', $formAllData) && !empty($formAllData['email'])) {
            $this->email = $formAllData['email'];
        } else {
            $_SESSION['errEmail'] = "Email requried";
        }

        //for Password pass to property
        if (array_key_exists('password', $formAllData) && !empty($formAllData['password'])) {
            $this->password = $formAllData['password'];
        } else {
            $_SESSION['errPassword'] = "Password required";
        }

        //for Repeat Password pass to property
        if (array_key_exists('repPassword', $formAllData) && !empty($formAllData['repPassword'])) {
            $this->repPassword = $formAllData['repPassword'];
        } else {
            $_SESSION['errRepPass'] = "Repeat password";
        }

        //for Session data pass
        $_SESSION['Name_v'] = $formAllData['userName'];
        $_SESSION['Email_v'] = $formAllData['email'];
        $_SESSION['Password_v'] = $formAllData['password'];
        $_SESSION['RePassword_v'] = $formAllData['repPassword'];
    }

    //end prepare methode
    //
    //
    //
    //
    //error massege minimizing method
    public function errHandling($errors='') {
        if (isset($_SESSION["$errors"]) && !empty($_SESSION["$errors"])) {
            echo $_SESSION["$errors"];
            unset($_SESSION["$errors"]);
        }
    }

    //data store methode
    public function signup() {
        if (!empty($this->userName) && !empty($this->email) && !empty($this->password) && !empty($this->repPassword)) {
            $verificationCode = uniqid();
            try {
                $insertQuery = "INSERT INTO `signup` (`id`, `uniqueId`, `verificationId`, `userName`, `password`, `email`, `isActive`, `isAdmin`, `created`, `modified`, `delete`) VALUES (:id, :uniqueId, :verificationId, :userName, :password, :email, :isActive, :isAdmin, :created, :modified, :delete)";
                $statement = $this->dbConnect->prepare($insertQuery);
                $statement->execute(array(
                    ':id' => null,
                    ':uniqueId' => uniqid(),
                    ':verificationId' => $verificationCode,
                    ':userName' => $this->userName,
                    ':password' => $this->password,
                    ':email' => $this->email,
                    ':isActive' => $this->isActive,
                    ':isAdmin' => $this->isAdmin,
                    ':created' => date("Y-m-d h:i:s"),
                    ':modified' => $this->modified,
                    ':delete' => $this->delete,
                ));
                
                $lastId=  $this->dbConnect->lastInsertId();
                
                $insertQuery = "INSERT INTO `tbl_profile` (`id`, `userId`) VALUES (:id, :userId)";
                $statement = $this->dbConnect->prepare($insertQuery);
                $statement->execute(array(
                    ':id' => null,
                    ':userId' => $lastId,
                ));
                
                
                $_SESSION['successMsg'] = "Data Submitted Successfully.";
                header("location:register.php?veryfiId=$verificationCode");
            } catch (PDOException $err) {
                echo'error' . $err->getMessage();
            }
        } else {
            header('location:create.php');
        }
    }

    //end store method
    //
    //
    //
    //
    //
    //
    //
    //email verification
    public function verify() {
        $verificationCode = "'" . $_GET['vid'] . "'";
        $selectQuery = "SELECT * FROM `signup` WHERE `verificationId` = $verificationCode";
        $stmt = $this->dbConnect->prepare($selectQuery);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (empty($row['verificationId'])) {
            $_SESSION['err_msg'] = "Invalid Registration";
            header('location:error.php');
        } else {
            if ($row['isActive'] == 1) {
                $_SESSION['err_msg'] = 'Email Already Verified.Go to <a href="login.php">login page</a>.';
                header('location:error.php');
            } else {
                try {
                    $updateQuery = "UPDATE `signup` SET `isActive` = '1' WHERE `signup`.`verificationId` = $verificationCode";
                    $stmt = $this->dbConnect->prepare($updateQuery);
                    $stmt->execute();
                    $_SESSION['verifiedMsg'] = "Registration Process Compleated. Now Login.";
                    header('location:login.php');
                } catch (PDOException $e) {
                    echo'error' . $e->getMessage();
                }
            }
        }
    }

    //end verify method
    //
    //
    //
    //
    public function login() {
        $userName = "'$this->userName'";
        $password = "'$this->password'";

        $selectQuery = "SELECT * FROM `signup` WHERE `userName` = $userName && `password`=$password";
        $stmt = $this->dbConnect->prepare($selectQuery);
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if (isset($user) && !empty($user)) {
            if ($user['isActive'] == 0) {
                $_SESSION['Message'] = "<h3>Your account not verified yet. Check your email and verify</h3>";
                header('location:login.php');
            } else {
                $_SESSION['user'] = $user;
                header('location:dashbord.php');
            }
        } else {
            $_SESSION['Message'] = "<h3>invalid username or password</h3>";
            header('location:login.php');
        }
    }

//
//    
//        end class ......
}

?>
