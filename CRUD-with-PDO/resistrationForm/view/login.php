<?php
include 'include/header.php';
include_once '../vendor/autoload.php';

use signupForm\resistrationForm\resistrationForm;

$object = new resistrationForm();


if (isset($_SESSION['verifiedMsg']) && !empty($_SESSION['verifiedMsg'])) {
    echo $_SESSION['verifiedMsg'];
    unset($_SESSION['verifiedMsg']);
}

if (isset($_SESSION['errMsg']) && !empty($_SESSION['errMsg'])) {
    echo $_SESSION['errMsg'];
    unset($_SESSION['errMsg']);
}
?>
<body>
    <h1 style="text-align: center"> Please login</h1>

    <p class="text-danger" style="font-weight: bold; font-size: 10px;">
        <?php
        if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
            echo $_SESSION['Message'];
            unset($_SESSION['Message']);
        }
        ?>
    </p>      

    <form style="width:30%; margin: auto;" action="loginProcess.php" method="POST">
        <div class="form-group">
            <label for="exampleInputEmail1">User Name</label>
            <input type="text" name="userName" class="form-control" placeholder="User Name" value=""/>
            <p class="text-danger" style="font-weight: bold; font-size: 10px;"><?php $object->errHandling('errName'); ?></p>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" placeholder="Password" value=""/>
            <p class="text-danger" style="font-weight: bold; font-size: 10px;"> <?php $object->errHandling('errPassword'); ?> </p>
        </div>

        <button type="submit" class="btn btn-default">Login</button>
    </form>

    <?php
    include 'include/footer.php';
    ?>
</body>
</html>