<?php
include 'include/header.php';
include_once '../vendor/autoload.php';

use signupForm\profileForm\profileForm;

$object = new profileForm();
$object->prepare($_GET);
$dbData = $object->singleUserRow();
//echo '<pre>';
//print_r($dbData);
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <!-- start form-->
            <form action="profileProcess.php" method="POST">
                <div class="form-group fName">
                    <label>Full Name</label>
                    <input type="text" name="fullName" class="form-control" value="<?php echo $dbData['fullName'] ?>" />
<!--                    <p class="text-danger" style="font-weight: bold; font-size: 10px;">dgser</p>-->
                </div>

                <div class="form-group fathers">
                    <label>Father's Name</label>
                    <input type="text" name="fatherName" class="form-control" value="<?php echo $dbData['fatherName'] ?>"/>
                </div>

                <div class="form-group mothers">
                    <label>Mother's Name</label>
                    <input type="text" name="motherName" class="form-control" value="<?php echo $dbData['motherName'] ?>"/>
                </div>

                <div class="form-group gender">
                    <label>Gender</label>
                    <label class="radio-inline">
                        <input type="radio" name="gender" id="inlineRadio1" value="male"> Male
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="gender" id="inlineRadio2" value="female"> Female
                    </label>
                </div>

                <div class="form-group birthDate">
                    <label>Date of Birth</label>
                    <input type="text" name="birthDate" class="form-control" placeholder="dd : mm : yy" value="<?php echo $dbData['birthDate'] ?>"/>
                </div>

                <div class="form-group mobile">
                    <label>Mobile Number</label>
                    <input type="text" name="mobileNum" class="form-control" value="<?php echo $dbData['mobile'] ?>"/>
                </div>

                <div class="form-group occupation">
                    <label>Occupation</label>
                    <input type="text" name="occupation" class="form-control" value="<?php echo $dbData['occupation'] ?>"/>
                </div>

                <div class="form-group eduStatus">
                    <label>Educational Status</label>
                    <input type="text" name="eduStatus" class="form-control" value="<?php echo $dbData['eduStatus'] ?>"/>
                </div>

                <div class="form-group religion">
                    <label>Religion</label>
                    <input type="text" name="religion" class="form-control" value="<?php echo $dbData['religion'] ?>"/>
                </div>

                <div class="form-group maridStatus">
                    <label>Marital Status</label>
                    <label class="radio-inline">
                        <input type="radio" name="metiralSts" id="inlineRadio1" value="single"> Single
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="metiralSts" id="inlineRadio2" value="married"> Married
                    </label>
                </div>

                <div class="form-group jobStatus">
                    <label>Current Job Status</label>
                    <input type="text" name="jobStatus" class="form-control" value="<?php echo $dbData['jobSts'] ?>"/>
                </div>

                <div class="form-group nationality">
                    <label>Nationality</label>
                    <input type="text" name="nationality" class="form-control" value="<?php echo $dbData['nationality'] ?>"/>
                </div>

                <div class="form-group bio">
                    <label>Bio</label>
                    <textarea name="bio" class="form-control" rows="3" value="<?php echo $dbData['bio'] ?>"></textarea>
                </div>

                <div class="form-group nid">
                    <label>National ID</label>
                    <input type="text" name="nationalId" class="form-control" value="<?php echo $dbData['nID'] ?>"/>
                </div>

                <div class="form-group passportId">
                    <label>Passport ID</label>
                    <input type="text" name="passportId" class="form-control" value="<?php echo $dbData['passportNum'] ?>"/>
                </div>

                <div class="form-group skill">
                    <label>Skill's</label>
                    <div class="checkbox">
                        <label><input type="checkbox" name="skills" value="html/css"> HTML/CSS</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="skills" value="graphic design"> Graphic Design </label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="skills" value="php"> PHP</label>
                    </div>
                </div>

                <div class="form-group language">
                    <label>Language</label>
                    <div class="checkbox">
                        <label><input type="checkbox" name="language" value="Bangla"> Bangla</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="language" value="Chines"> Chines </label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="language" value="English"> English</label>
                    </div>
                </div>

                <div class="form-group interest">
                    <label>Interest</label>
                    <div class="checkbox">
                        <label><input type="checkbox" name="interest" value="traveling"> Traveling</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="interest" value="gym"> Gym </label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="interest" value="reading book"> Reading Book</label>
                    </div>
                </div>

                <div class="form-group profilePic">
                    <label>Profile Picture</label>
                    <input type="text" name="profilePic" class="form-control" value=""/>
                </div>

                <div class="form-group bladGroup">
                    <label>Blood Group</label>
                    <select name="bloodGrp" class="form-control" required>
                        <option value="">Select one</option>
                        <option value="o+ve">O(+ve)</option>
                        <option value="o-ve">O(-ve)</option>
                        <option value="a+ve">A(+ve)</option>
                        <option value="a-ve">A(-ve)</option>
                        <option value="ab+ve">AB(+ve)</option>
                    </select>
                </div>

                <div class="form-group fax">
                    <label>Fax Number</label>
                    <input type="text" name="faxNum" class="form-control" value="<?php echo $dbData['faxNum'] ?>"/>
                </div>

                <div class="form-group height">
                    <label>Height (meter)</label>
                    <input type="text" name="height" class="form-control" value="<?php echo $dbData['height'] ?>"/>
                </div>

                <div class="form-group address">
                    <label>Address</label>
                    <input type="text" name="addres" class="form-control" value="<?php echo $dbData['address'] ?>"/>
                </div>
                <div class="form-group zipCode">
                    <label>Zip Code</label>
                    <input type="text" name="zipCode" class="form-control" value=""/>
                </div>
                <div class="form-group city">
                    <label>City</label>
                    <input type="text" name="city" class="form-control" value=""/>
                </div>

                <div class="form-group webUrl">
                    <label>Website URL</label>
                    <input type="text" name="webUrl" class="form-control" value="<?php echo $dbData['webUrl'] ?>"/>
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <!-- end form-->
        </div>
    </div>
</div>












<?php
include 'include/footer.php';
?>




