<?php
class db extends mysqli{
	
	public $sql,$data = array(), $link;
	
	protected $dbhost = "localhost";
	protected $dbuser = "root";
	protected $dbpass = "";
	protected $dbname = "omms";
	
	public function __construct(){
		$this->link = parent::__construct($this->dbhost, $this->dbuser, $this->dbpass, $this->dbname);
		  if ($this->connect_error) {
            die('Connect Error (' . $this->connect_errno . ') '
                    . $this->connect_error);
		}
		//echo 'Success... ' . $this->host_info . "\n";
	}
	
	public function select_data($tablename, $field='*', $where='', $order=''){
		$this->sql = "SELECT $field FROM $tablename ";
		if(null != $where) {
			$this->sql .= "WHERE $where";
		}
		if(null != $order) {
			$this->sql .= "ORDER BY $order";
		}
		$this->data = array();
		if($res = $this->query($this->sql)){
			while($data =  $res->fetch_assoc()){
				$this->data[] = $data;
			}
		}
		else {
			return false;
		}
		return $this->data;
	}
	function insert_data($table, $keyval) {
		$values = "";
		$fields = implode(',',array_keys($keyval));
		foreach($keyval as $value) {
			$values .= ($values == "")?"":",";
			$values .= "'".$value."'";
		} 
		$this->sql = "INSERT INTO $table($fields) VALUES($values)";
		if($query = $this->query($this->sql)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	function insert_data1($table, $keyval) {
		$values = "";
		$fields = implode(',',array_keys($keyval));
		$values = implode("','",array_values($keyval));
		$values = "'".$values."'";
		$this->sql = "INSERT INTO $table($fields) VALUES($values)";
		if($query = $this->query($this->sql)) {
			return true;
		}
		else {
			return $this->error;
		}
	}
	
	function update_data($table, $data, $where) {
	
		$fieldWithValue = "";
		
		foreach($data as $key=>$value) {
			if(is_string($value)) {
				$value = "'$value'";
			}
			$fieldWithValue .= $key . "=" . $value . ",";
		}
		
		$fieldWithValue = rtrim($fieldWithValue, ',');
		$this->sql = "UPDATE $table SET $fieldWithValue WHERE $where";
		
		if($query = $this->query($this->sql)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	function delete_data($table, $where) {
		$this->sql = "DELETE FROM $table WHERE $where";
		if($query = $this->query($this->sql)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	function join_data($table1, $table2) {
		$this->sql = "SELECT SUM($table2.amount) as amount, $table2.user_id, $table1.name, $table2.collection_date 
						FROM $table2 INNER JOIN $table1 ON 
						$table1.id = $table2.user_id GROUP BY $table2.user_id";
		$this->data = array();
		if($res = $this->query($this->sql)){
			while($data =  $res->fetch_assoc()){
				$this->data[] = $data;
			}
		}
		else {
			return false;
		}
		return $this->data;
	}
	function sum_data($table, $where='', $group='') {
		$this->sql = "SELECT sum(amount) as monthlyamount, amount, collection_date, user_id FROM $table";
		if(null != $where) {
			$this->sql .= "WHERE $where ";
		}
		if(null != $group) {
			$this->sql .= "GROUP BY user_id";
		}
		$this->data = array();
		if($res = $this->query($this->sql)){
			while($data =  $res->fetch_assoc()){
				$this->data[] = $data;
			}
		}
		else {
			return false;
		}
		return $this->data;
	}
}