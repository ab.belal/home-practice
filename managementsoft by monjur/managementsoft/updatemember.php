<?php
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	if($_SESSION['user']['type'] != 1) {
		header('Location: error.php');
	}
	else {
		require_once('header.php');
		require_once('include/db.php');
		require_once('include/validator.php');
		$db = new db();
		$valid = new validator();
		if(isset($_GET['userid'])) {
			$id = $_GET['userid'];
		}
		if(isset($_POST['update'])) {
			$name = $valid->sanitize($_POST['name'], true);
			$email = $valid->sanitize($_POST['email'], true);
			$mobile = $valid->sanitize($_POST['mobile'], true);
			$status = $valid->sanitize($_POST['status'], true);
			$data = array(
				'name' => array(
					'value' => $name,
					'type' => 'text',
					'min' => 1,
					'max' => 50
				),
				'email' => array(
					'value' => $email,
					'type' => 'email',
				),
				'mobile' => array(
					'value' => $mobile,
					'type' => 'number',
					'min' => 11,
					'max' => 11
				)
			);
			$getdata = array();
			$getdata = $valid->getData($data);
			if (count($getdata) == 0) {
				$fieldwithvalue = array(
					'name' => $name,
					'email' => $email,
					'mobile' => $mobile,
					'status' => $status
				);
				$where = "id = $id";
				$update = $db->update_data('users', $fieldwithvalue, $where);
				if($update) {
					$added = "Data Successfully Updated";
				}
			}
			foreach($getdata as $val) {
				$output .= "<p class='output'>$val</p>";
			}
		}
?>
			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Blank</a>
					</li>
				</ul>
			</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-picture"></i>Blank</h2>
						<h2>
							<?php
								if(isset($added) && $added != null) {
									echo $added;
								}
							?>
						</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?php
							$where = "id = $id";
							$users = $db->select_data('users', '*', $where);
							$row = count($users);
							
						?>
						<form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
							<table class="table">
								<tr>
									<td>Name</td>
									<td>
										<input value="<?php echo $users[0]['name']; ?>" name="name" class="input-xlarge focused" id="focusedInput" type="text" />
									</td>
								</tr>
								<tr>
									<td>Email</td>
									<td><input name="email" value="<?php echo $users[0]['email']; ?>" class="input-xlarge focused" id="focusedInput" type="email" /></td>
								</tr>
								<tr>
									<td>Mobile</td>
									<td><input value="<?php echo $users[0]['mobile']; ?>" name="mobile" type="text" class="input-xlarge" id="number" /></td>
								</tr>
								<tr>
									<td>Status</td>
									<td>
										<?php
										$checkeda = "";
										$checkedi = "";
										if($users[0]['status'] == 0) {
											$checkedi = "checked";
										}
										if($users[0]['status'] == 1) {
											$checkeda = "checked";
										}
									?>
										<label class="radio">
											<input <?php echo $checkeda ?> type="radio" name="status" id="status" value="1">
											Active
										</label>
										<label class="radio">
											<input <?php echo $checkedi ?> type="radio" name="status" id="status" value="0">
											Inactive
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<input class="btn btn-default" type="submit" name="update" value="Update" />
									</td>
								</tr>
							</table>
						</form>
					</div>
				</div><!--/span-->
			</div><!--/row-->
    
<?php 
	include('footer.php');
}
?>
