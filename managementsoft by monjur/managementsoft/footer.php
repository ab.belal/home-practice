		<?php if(!isset($no_visible_elements) || !$no_visible_elements)	{ ?>
			<!-- content ends -->
			</div><!--/#content.span10-->
		<?php } ?>
		</div><!--/fluid-row-->
		<?php if(!isset($no_visible_elements) || !$no_visible_elements)	{ ?>
		
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<footer>
			<p class="pull-left">&copy; Muhammad Ali Akbar | <?php echo date('Y') ?></p>
		</footer>
		<?php } ?>

	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- jQuery -->
	<script src="js/jquery-1.7.2.min.js"></script>
	<!-- jQuery UI -->
	<script src="js/jquery-ui-1.8.21.custom.min.js"></script>
	<!-- transition / effect library -->
	<script src="js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script src="js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script src="js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script src="js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script src="js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script src="js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script src="js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script src="js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script src="js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src="js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src="js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script src="js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script src="js/bootstrap-tour.js"></script>
	<!-- library for cookie management -->
	<script src="js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script src='js/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='js/jquery.dataTables.min.js'></script>

	<!-- chart libraries start -->
	<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.min.js"></script>
	<script src="js/jquery.flot.pie.min.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script src="js/jquery.chosen.min.js"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="js/jquery.uniform.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="js/jquery.colorbox.min.js"></script>
	<!-- rich text editor library -->
	<script src="js/jquery.cleditor.min.js"></script>
	<!-- notification plugin -->
	<script src="js/jquery.noty.js"></script>
	<!-- file manager library -->
	<script src="js/jquery.elfinder.min.js"></script>
	<!-- star rating plugin -->
	<script src="js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="js/jquery.history.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox.js"></script>
	<!-- application script for Charisma demo -->
	<script src="js/charisma.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			
			function check_form(str) {
				if(str == "" || str == null) {
					alert("null field not allowed");
					return false;
				}
				else {
					return true;
				}
			}
			
			var add_tr_in_table         = $(".add_field_row"); 
			var list_button      = $(".create_list"); 
			var add_submit_button = $(".add_submit_button");
			
			$(list_button).click(function(e){ 
				e.preventDefault();
				var costdate = $('#date').val();
				var amount = document.getElementById("amount").value;
				var price = document.getElementById("price").value;
				var totalprice = document.getElementById("totalcost").value;
				var discount = document.getElementById("discount").value;
				var productid = document.getElementById("product").value;
				var text = $("#product option:selected").html();
				var type = $("input[name=member]:checked").val();
				var member = $("#member").val();
				counter = 1;

				if(counter == 1) {
					$(".hidedatemember").hide();
				}

				if(type == "member") {
					type = member;
				}

				$(".datewithuser").html("Bazzar List Created by " + type + "On " + costdate + "<a href='#' class='update'>Update</a><input type='hidden' value='"+costdate+"' name='date' /><input type='hidden' value='"+type+"' name='member' />");

				if(rtrn = check_form(amount)) {
					if(rtrn = check_form(price)) {
						if(rtrn = check_form(discount)){
							$(add_tr_in_table).append('<tr><td><input value="'+productid+'" type="hidden" name="productid[]" class="form-control" readonly /><input value="'+text+'" type="text" name="product[]" class="form-control" readonly /></td><td><input type="number" value="'+amount+'" name="quantity[]" class="form-control" readonly /></td><td><input type="number" value="'+price+'" name="unit_price[]" class="form-control" readonly /></td><td><input type="number" value="'+totalprice+'" name="price[]" class="form-control" readonly /><a href="#" class="remove_field">Remove</a></td></tr>');
							$(add_submit_button).html('<input value="Save" name="savecost" class="btn btn-default" type="submit" />');
						}
					}
				}
				
			});
			
			$(add_tr_in_table).on("click",".remove_field", function(e){ 
				e.preventDefault(); 
				$(this).parent().remove(); 
			});
		
			$("a#inline").fancybox({
				'width'  : 600,
				'autoSize' : false
			});
			
			$(".viewall").fancybox({
				'width'  : 400,
				'autoSize' : false
			});
			
			$(".member_details").fancybox({
				'width'  : 400,
				'autoSize' : false
			});
			
			$("a#newfood").fancybox({
				'width'  : 300,
				'autoSize' : false
			});
			
			$('#sbmt').attr('disabled', true);
			
			$("#repass").blur(function(){
			var pass = $("#pass").val();
			var repass = $("#repass").val();
				if(pass !== "") {
					if(pass !== repass) {
						alert("password does not match");
						$('#sbmt').attr('disabled', true);
					}
					else {
						$('#sbmt').attr('disabled', false);
					}
				}
			});
			
			$("#repeatmobile").blur(function(){
			var mobile = $("#mobile").val();
			var rptmobile = $("#repeatmobile").val();
				if(pass !== "") {
					if(mobile !== rptmobile) {
						alert("mobile no does not match");
						$('#sbmt').attr('disabled', true);
					}
					else {
						$('#sbmt').attr('disabled', false);
					}
				}
			});
		
			var currentTime = new Date();
			var year = currentTime.getFullYear();
			document.getElementById("year").value = year;
			
			$("#minus").click(function() {
				var year =  parseInt($('#year').val()); 
				year = year-1; 
				$('#year').val(year);
			});
			
			$("#plus").click(function() {
				var year =  parseInt($('#year').val()); 
				year = year+1; 
				$('#year').val(year);
			});
			
			$('.jh').hide();
			
			$('#member1').click(function(){
				$( ".inner" ).hide();
			});
					
			$('#member2').click(function(){
				$( ".inner" ).show();
			});
			
			
		});
	</script>
	<script src="js/omms.js"></script>
	<?php //Google Analytics code for tracking my demo site, you can remove this.
		if($_SERVER['HTTP_HOST']=='usman.it') { ?>
		<script>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-26532312-1']);
			_gaq.push(['_trackPageview']);
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
			})();
			
		</script>
	<?php } ?>
	
</body>
</html>
