<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Mess Management System</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
	<meta name="author" content="Muhammad Usman">

	<!-- The styles -->
	<link id="bs-css" href="css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/charisma-app.css" rel="stylesheet">
	<link href="css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href='css/fullcalendar.css' rel='stylesheet'>
	<link href='css/fullcalendar.print.css' rel='stylesheet'  media='print'>
	<link href='css/chosen.css' rel='stylesheet'>
	<link href='css/uniform.default.css' rel='stylesheet'>
	<link href='css/colorbox.css' rel='stylesheet'>
	<link href='css/jquery.cleditor.css' rel='stylesheet'>
	<link href='css/jquery.noty.css' rel='stylesheet'>
	<link href='css/noty_theme_default.css' rel='stylesheet'>
	<link href='css/elfinder.min.css' rel='stylesheet'>
	<link href='css/elfinder.theme.css' rel='stylesheet'>
	<link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
	<link href='css/opa-icons.css' rel='stylesheet'>
	<link href='css/uploadify.css' rel='stylesheet'>
	<link type="text/css" href='css/omms.css' rel='stylesheet'>
	<link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<script type="text/javascript" src="js/function.js"></script>
</head>

<body>
	<?php if(!isset($no_visible_elements) || !$no_visible_elements)	{ ?>
	<!-- topbar starts -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="members.php"><span>Mess Management</span></a>
				
				<!-- theme selector starts -->
				<?php
					if($_SESSION['user']['type'] == 1) {
						$usertype = 'Admin';
					}
					else if($_SESSION['user']['type'] == 2) {
						$usertype = 'Manager';
					}
					else if($_SESSION['user']['type'] == 3) {
						$usertype = 'Member';
					}
				if($_SESSION['user']['type'] == 1) {
				?>
				<div class="btn-group pull-right theme-container" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-tint"></i><span class="hidden-phone"> Change Theme / Skin</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" id="themes">
						<li><a data-value="classic" href="#"><i class="icon-blank"></i> Classic</a></li>
						<!--<li><a data-value="cerulean" href="#"><i class="icon-blank"></i> Cerulean</a></li>
						<li><a data-value="cyborg" href="#"><i class="icon-blank"></i> Cyborg</a></li>
						<li><a data-value="redy" href="#"><i class="icon-blank"></i> Redy</a></li>
						<li><a data-value="journal" href="#"><i class="icon-blank"></i> Journal</a></li>
						<li><a data-value="simplex" href="#"><i class="icon-blank"></i> Simplex</a></li>
						<li><a data-value="slate" href="#"><i class="icon-blank"></i> Slate</a></li>
						<li><a data-value="spacelab" href="#"><i class="icon-blank"></i> Spacelab</a></li>
						<li><a data-value="united" href="#"><i class="icon-blank"></i> United</a></li>-->
					</ul>
				</div>
				<?php
				}
				?>
				<!-- theme selector ends -->
				
				<!-- user dropdown starts -->
				<div class="btn-group pull-right" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user"></i><span class="hidden-phone"><?php echo $usertype; ?></span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#">Profile</a></li>
						<li class="divider"></li>
						<li><a href="changepassword.php">Change Password</a></li>
						<li class="divider"></li>
						<li><a href="log/logout.php">Logout</a></li>
					</ul>
				</div>
				<!-- user dropdown ends -->
			</div>
		</div>
	</div>
	<!-- topbar ends -->
	<?php } ?>
	<div class="container-fluid">
		<div class="row-fluid">
		<?php if(!isset($no_visible_elements) || !$no_visible_elements) { ?>
		
			<!-- left menu starts -->
			<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						
						<li>
							<h2 style="text-align: center;background: #126DB3; color: #fff; font-size: 16px">Member</h2>
								<li>
									<a class="ajax-link" href="members.php">
										<span class="hidden-tablet">View Member</span>
									</a>
								</li>
								<?php 
								if($_SESSION['user']['type'] == 1) {
								?>
								<li>
									<a class="ajax-link" href="addmember.php">
										<span class="hidden-tablet">Add Member</span>
									</a>
								</li>
								<li>
									<a class="ajax-link" href="addmanager.php">
										<span class="hidden-tablet">Create Manager</span>
									</a>
								</li>
								<?php 
								}
								?>
						</li>
						<li> 
							<h2 style="text-align: center;background: #126DB3; color: #fff; font-size: 16px">Deposit</h2>
	
							<li> 
								<a href='viewcollection.php' title='Traffic'>View Deposit</a>
							</li>
							<li> 
								<a href='collectmoney.php' title='Conversion rate'>Add Deposit</a>
							</li>
						
						</li>
						<li> 
							<h2 style="text-align: center;background: #126DB3; color: #fff; font-size: 16px">Meal</h2>
							<li> 
								<a href='viewmeal.php' title='Traffic'>View Meal</a>
							</li>
							<li> 
								<a href='mealentry.php' title='Conversion rate'>Insert Meal</a>
							</li>
						</li>
						<li>
							<h2 style="margin-bottom: 5px;text-align: center;background: #126DB3; color: #fff; font-size: 16px"><a style="color: #fff;" href="viewfood.php">View Food</a></h2>
						</li>
						<li>
							<h2 style="text-align: center;background: #126DB3; color: #fff; font-size: 16px">Cost</h2>
							<li>
								<a class="ajax-link" href="insertcost.php">
									<span class="hidden-tablet">Add Cost</span>
								</a>
							</li>
							<li>
								<a class="ajax-link" href="view_member_cost.php">
									<span class="hidden-tablet">View Member Cost</span>
								</a>
							</li>
							<li>
								<a class="ajax-link" href="view_manager_cost.php">
									<span class="hidden-tablet">View Manager Cost</span>
								</a>
							</li>
						</li>
						<li>
							<h2 style="text-align: center;background: #126DB3; color: #fff; font-size: 16px"><a style="color: #fff;" href="finalreport.php">Final Report</a></h2>
						</li>
						
					</ul>
					
				</div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			<?php } ?>
