<?php 
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	else {
		require_once('header.php');
		require_once('include/db.php');
		$database = new db();
		$num_day = date('t');
		$cur_date = date('Y-m');
?>


			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Meal</a>
					</li>
				</ul>
			</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-picture"></i>View Meal</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?php
							$message = "";
							if(isset($_POST['update'])) {
								$mealid = $_POST['mealid'];
								$where = "id = '$mealid'";
								$total_meal = $_POST['morning'] + $_POST['lunch'] + $_POST['dinner'] + $_POST['guest'];
								$meal = array(
									'morning' => $_POST['morning'],
									'afternoon' => $_POST['lunch'],
									'night' => $_POST['dinner'],
									'guest' => $_POST['guest'],
									'total_meal' => $total_meal
								);
								if($mealupdate = $database->update_data('meal_entry', $meal, $where)) {
									$message = "Meal Successfully Updated";
								}
								else {
									$message = "Error has been occured";
								}
							}
							if(isset($_GET['id'])) {
								$where = "id = ".$_GET['id'];
								if($database->delete_data('meal_entry', $where)) {
									$message = "Meal Successfully Deleted";
								}
								else {
									$message = "Error has been occured";
								}
							}
							echo "<h2 style='text-align: center'>{$message}</h2>";
						?>
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
							<thead>
								<tr>
									<th>Name/Date</th>
									
									<?php 
										for($i=1;$i<=$num_day;$i++){
											echo "<th> $i </th>";
										}
									?>
								</tr>
							</thead>
	
							<?php
								$users = $database->select_data("users", '*', 'status = 1');
								$user_num = count($users);
								$fancid = 0;
								for($i = 0; $i < $user_num; $i++) {
									echo '<tr>';
									echo "<td>{$users[$i]['name']}</td>";
									for($checkdate = 1; $checkdate <= $num_day; $checkdate++) {
										$date = $cur_date."-$checkdate";
										$where = "user_id = '{$users[$i]['id']}' and date = '{$date}'";
										$count_meal = $database->select_data("meal_entry", '*', $where);
										$countrow = count($count_meal);
										$counter = 0;
										for($j = 0; $j < $countrow; $j++) {
											$fancid++;
											echo "<td>
												<a class='viewall' href='#data{$fancid}'>{$count_meal[$j]['total_meal']}</a>
			
												<div style='display:none;' id='data{$fancid}'>
								
													<table class='table table-bordered'>
							
														<tr>
															<td>Date</td>
															<td>{$date}</td>
														</tr>
														<tr>
															<td>Breakfast</td>
															<td>{$count_meal[$j]['morning']}</td>
														</tr>
														<tr>
															<td>Lunch</td>
															<td>{$count_meal[$j]['afternoon']}</td>
														</tr>
														<tr>
															<td>Dinner</td>
															<td>{$count_meal[$j]['night']}</td>
														</tr>
														<tr>
															<td>Guest</td>
															<td>{$count_meal[$j]['guest']}</td>
														</tr>
														<tr>
															<td>Total Meal</td>
															<td>{$count_meal[$j]['total_meal']}</td>
														</tr>
													</table>
	
													<a class='viewall btn btn-primary' href='#hello{$fancid}'>Update</a>
													<a class='btn btn-danger' href='{$_SERVER['PHP_SELF']}?id={$count_meal[$j]['id']}'>Delete</a>
												</div>
												<div style='display:none;' id='hello{$fancid}'>
													<form action='#' method='post'>
														<input type='hidden' name='mealid' value='{$count_meal[$j]['id']}' />
														<table class='border'>
															<tr>
																<th>Morning</th>
																<th><input name='morning' type='number' value='{$count_meal[$j]['morning']}' /></th>
															</tr>
															<tr>
																<th>Lunch</th>
																<th><input name='lunch' type='number' value='{$count_meal[$j]['afternoon']}' /></th>
															</tr>
															<tr>
																<th>Dinner</th>
																<th><input name='dinner' type='number' value='{$count_meal[$j]['night']}' /></th>
															</tr>
															<tr>
																<th>Guest</th>
																<th><input name='guest' type='number' value='{$count_meal[$j]['guest']}' /></th>
															</tr>
														</table>
														<input class='btn btn-primary' type='submit' name='update' value='Update' />
													</form>
												</div>
											</td>";
											$counter++;
										}
										$fancid++;
										if($counter == 0) {
											echo "<td>0</td>";
										}
									}
									echo '</tr>';
								}
							?>
		
		
							</tr>	
						</table>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

    
<?php
	}
	include('footer.php'); 
?>
