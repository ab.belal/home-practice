<?php 
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	else {
		require_once('header.php');
		require_once('include/db.php');
		require_once('include/validator.php');
		$db = new db();
		$valid = new validator();
		if(isset($_POST['collect'])) {
			$memberid = $valid->sanitize($_POST['membername'], true);
			$amount = $valid->sanitize($_POST['amount'], true);
			$date = $valid->sanitize($_POST['date'], true);
			$date = explode('/', $date);
			$date = $date[2].'-'.$date[0].'-'.$date[1];
			$data = array(
				'membername' => array(
					'value' => $memberid,
					'type' => 'number',
					'min' => 1,
					'max' => 5
				),
				'amount' => array(
					'value' => $amount,
					'type' => 'number',
					'min' => 1,
					'max' => 4
				)
			);
			$getdata = array();
			$getdata = $valid->getData($data);
			if (count($getdata) == 0) {
				$fieldwithvalue = array(
					'user_id' => $memberid,
					'amount' => $amount,
					'collection_date' => $date,
					'collection_by' => $_SESSION['user']['id']
				);
				$insert = $db->insert_data1('collection', $fieldwithvalue);
				if($insert) {
					$added = "Money Collected";
				}
			}
			foreach($getdata as $val) {
				$output .= "<p class='output'>$val</p>";
			}
		}
?>
			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Deposit</a>
					</li>
				</ul>
			</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-picture"></i>Add Deposit</h2>
						<h2>
							<?php
								if(isset($added) && $added != null) {
									echo $added;
								}
							?>
						</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form method="post" action="">
							<table class="table">
								<tr>
									<td>Name</td>
									<td>
										<select name="membername" id="selectError3">
											<option value="">Select a member</option>
											<?php
												$users = $db->select_data('users', '*', 'status = 1');
												$row = count($users);
												for($i = 0; $i < $row; $i++) {
													echo "<option value='".$users[$i]['id']."'>".$users[$i]['name']."</option>";
												}
											?>
										</select>
									</td>
								</tr>
								<tr>
									<td>Amount</td>
									<td><input name="amount" class="input-xlarge focused" id="focusedInput" type="number" /></td>
								</tr>
								<tr>
									<td>Date</td>
									<td><input name="date" type="text" class="input-xlarge datepicker" id="date" /></td>
								</tr>
								<tr>
									<td><input class="btn btn-default" type="submit" value="Save" name="collect" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div><!--/span-->
			</div><!--/row-->
    
<?php 
	include('footer.php');
}
?>
