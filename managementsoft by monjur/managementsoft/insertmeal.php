<?php
	require_once('session.php');
	require_once('include/db.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	else {
		if(isset($_POST['submit'])){
			$meal = new db();
			$rowcount = count($user);
			$count = count($_POST['name']);
			for($row = 0; $row < $count; $row++) {
				$id = $_POST['name'][$row];
				$date = $_POST['date'];
				$arr_date = explode('/',$date);
				$main_date = "{$arr_date[2]}-{$arr_date[0]}-{$arr_date[1]}";
				
				$morning = $_POST['bf'][$row];
				$afternoon = $_POST['ln'][$row];
				$night = $_POST['dn'][$row];
				$guest = (($_POST['bfguest'][$row]/2) + $_POST['lnguest'][$row] + ($_POST['dnguest'][$row]/2));
				$total = $morning + $afternoon + $night + $guest;
				$data = array(
					'user_id' => $id,
					'date' => $main_date,
					'morning' => $morning,
					'afternoon' => $afternoon,
					'night' => $night,
					'guest' => $guest,
					'total_meal' => $total
				);
				if($meal->insert_data1('meal_entry', $data)) {
					$report = "data successfully inserted";
				}
				else {
					$report = mysql_error();
				}
			}
			header("Location: viewmeal.php");
		}
		else {
			header('Location: mealentry.php');
		}
	}
	mysql_real_escape_string("hello");
?>