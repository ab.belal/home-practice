<?php
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	else {
		require_once('include/db.php');
		require_once('include/validator.php');
		if(isset($_POST['register'])) {
			$valid = new validator();
			$name = $valid->sanitize($_POST['name'], true);
			$mobile = $valid->sanitize($_POST['mobile'], true);
			$rptmobile = $valid->sanitize($_POST['repeatmobile'], true);
			$email = $valid->sanitize($_POST['email'], true);
			$password = $valid->sanitize($_POST['password']);
			$rptpassword = $valid->sanitize($_POST['repassword']);
			$date = $valid->sanitize($_POST['regdate'], true);
			$date = explode('/', $date);
			$date = $date[2].'-'.$date[0].'-'.$date[1];
			
			if($mobile != $rptmobile) {
				array_push($confirm, "Mobile no doesn't match");
			}
			$data = array(
				'name' => array(
					'value' => $name,
					'type' => 'text',
					'min' => 5,
					'max' => 50
				),
				'email' => array(
					'value' => $email,
					'type' => 'email'
				),
				'password' => array(
					'value' => $password,
					'type' => 'text',
					'min' => 8,
					'max' => 50
				),
				'mobile' => array(
					'value' => $mobile,
					'type' => 'number',
					'min' => 11,
					'max' => 11
				)
			);
			$output = '';
			if($password == $rptpassword &&  $mobile == $rptmobile) {
				$errors = $valid->getData($data);
			}
			else {
				echo "confirm mobile or password";
			}
			if (count($errors) == 0) {
				$fieldwithvalue = array(
					'name' => $name,
					'email' => $email,
					'password' => md5($password),
					'mobile' => $mobile,
					'date' => $date,
					'status' => 1,
					'type' => 3
				);
				$database = new db();
				$check = array();
				$where = "mobile = $mobile";
				$check = $database->select_data('users', '*', $where);
				if(count($check) > 0) {
					header("Location: addmember.php?error=".$mobile." this number already registered");
				}
				else {
					$insert = $database->insert_data1('users', $fieldwithvalue);
					if($insert) {
						$success = "New Member added";
						header("Location: members.php?message=".$success);
					}
				}
			}
			foreach($errors as $val) {
				$output .= "<p class='output'>$val</p>";
				header("Location: addmember.php?error=".$output);
			}
		}
	}
?>