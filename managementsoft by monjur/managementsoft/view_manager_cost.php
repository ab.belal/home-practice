<?php
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	else {
		require_once('header.php');
		require_once('include/db.php');
		$database = new db();
?>


			<div id="adminbar">
				<ul style="float: left" class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Cost</a>
					</li>
				</ul>
				<ul style="float: right" class="breadcrumb">
					<li>
						<?php echo $_SESSION['user']['name']; ?><span class="divider">|</span>
					</li>
					<?php
						if($_SESSION['user']['type'] == 1) {
							$usertype = 'Admin';
						}
						else if($_SESSION['user']['type'] == 2) {
							$usertype = 'Manager';
						}
						else if($_SESSION['user']['type'] == 3) {
							$usertype = 'Member';
						}
					?>
					<li>
						<?php echo $usertype; ?>
					</li>
				</ul>
			</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-picture"></i>View Manager Cost</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
							<select name="month" id="">
								<option value="">---Select Month---</option>
								<?php
									require_once('include/function.php');
									$search_with_month = getMonthArray();
									foreach($search_with_month as $key=>$value) {
										echo "<option value='{$key}'>{$value}</option>";
									}
								?>
							</select>
							<select name="year" id="">
								<option value="">---Select Year---</option>
								<?php
									for($i = 2014; $i > 2010; $i--) {
										echo "<option value='{$i}'>{$i}</option>";
									}
								?>
							</select>
							<input class="btn btn-primary" type="submit" name="search" value="Search" />
						</form>
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						<thead>
							<tr>
								<th>Id</th>
								<th>Product Name</th>
								<th>Quantity</th>
								<th>Unit Price</th>
								<th>Total Price</th>
								<th>Date</th>
							</tr>
						</thead>
						<?php 
							if(isset($_POST['search'])) {
								$cur_date = $_POST['year'].'-'.$_POST['month'];
								echo $cur_date;
							}
							else {
								$cur_date = date('Y-m');
							}
							$num_day = date('t');
							$sl = 0;
							$database->sql = "SELECT users.name as membername, fl.name as foodname, mc.* FROM manager_cost as mc inner join food_list as fl 
							on mc.product_id = fl.id inner join users on users.id = mc.user_id where creation_date
							LIKE '{$cur_date}-%'";
							if($res = $database->query($database->sql)){
								while($data =  $res->fetch_assoc()){
									$sl++;
						?>
							<tr>
								<td><?php echo $sl;?></td>
								<td><?php echo $data['foodname'];?></td>
								<td><?php echo $data['quantity'];?></td>
								<td><?php echo $data['unit_price'];?></td>
								<td><?php echo $data['total_price'];?></td>
								<td><?php echo $data['creation_date'];?></td>
							</tr>
						<?php 
									}
								}
						?>
						</table>
					</div>
				</div><!--/span-->
			</div><!--/row-->
<?php 
	require_once('footer.php');
	}
?>
