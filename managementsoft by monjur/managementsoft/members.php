<?php
	require_once('session.php');
	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
	}
	require_once('header.php');
	require_once('include/db.php');
?>
			<div id="adminbar">
				<ul style="float: left" class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">All Members</a>
					</li>
				</ul>
				<ul style="float: right" class="breadcrumb">
					<li>
						<?php echo $_SESSION['user']['name']; ?><span class="divider">|</span>
					</li>
					<?php
						if($_SESSION['user']['type'] == 1) {
							$usertype = 'Admin';
						}
						else if($_SESSION['user']['type'] == 2) {
							$usertype = 'Manager';
						}
						else if($_SESSION['user']['type'] == 3) {
							$usertype = 'Member';
						}
					?>
					<li>
						<?php echo $usertype; ?>
					</li>
				</ul>
			</div>
			
			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Members</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?php
							$user = new db();
							if(isset($_GET['id'])) {
								$where = "id = ".$_GET['id'];
								$data = array(
									'status' => 0
								);
								$update = $user->update_data('users', $data, $where);
								if($update) {
									echo "1 member deleted";
								}
							}
							if(isset($_GET['message'])) {
								echo "<h3 style='text-align:center'>{$_GET['message']}</h3>";
							}
							if($_SESSION['user']['type'] == 1) {
						?>
							<div id="add-member" style="margin-bottom: 10px">
								<form action="#" method="post">
									<button name="current_member" type="submit" class="btn btn-primary">Current</button>
									<button name="old_member" type="submit" class="btn btn-primary">Old</button>
								</form>
							</div>
						<?php
							}
						?>
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Name</th>
								  <th>Date registered</th>
								  <th>Email</th>
								  <th>Mobile</th>
								  <th>Status</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
							<?php
								if($_SESSION['user']['type'] == 1) {
									if(isset($_POST['current_member'])) {
										$viewuser = $user->select_data("users",'*','status = 1');
									}
									else if(isset($_POST['old_member'])) {
										$viewuser = $user->select_data("users",'*','status = 0');
									}
									else {
										$viewuser = $user->select_data("users",'*','','status DESC');
									}
								}
								else {
									$viewuser = $user->select_data("users",'*','status = 1');
								}
								$user_num = count($viewuser);
								$fancid = 0;
								for($i = 0; $i < $user_num; $i++) {
									$fancid++;
									echo '<tr>';
									echo "<td>{$viewuser[$i]['name']}</td>";
									echo "<td class='center'>{$viewuser[$i]['date']}</td>";
									echo "<td class='center'>{$viewuser[$i]['email']}</td>";
									echo "<td class='center'>{$viewuser[$i]['mobile']}</td>";
									if($viewuser[$i]['status'] == 1) {
										$status = "Active";
										echo "<td class='center'>
											<span class='label label-success'>{$status}</span>
										</td>";
									}
									else if($viewuser[$i]['status'] == 0) {
										$status = "Inctive";
										echo "<td class='center'>
											<span class='label'>{$status}</span>
										</td>";
									}
									?>
									<td class="center">
										<a class="btn btn-success member_details" href="#viewmember<?php echo $fancid; ?>">
											<i class="icon-zoom-in icon-white"></i>  
											View                                            
										</a>
										
									<?php
									echo "<div style='display:none;' id='viewmember{$fancid}'>
											<table width='100%'>
												<thead >
													<tr > 
														<th colspan='2'>View Member Details</th>
													</tr>
												</thead>
												<tbody>
													<tr style='border-bottom:1px solid #d0d0d0; height:40px; font-size:18px; font-weight:bold;'> 
														<td> User Id :</td> <td> {$viewuser[$i]['id']}</td>
													</tr>
													<tr style='border-bottom:1px solid #d0d0d0; height:40px; font-size:18px; font-weight:bold;'> 
														<td> Name :</td> <td> {$viewuser[$i]['name']}</td>
													</tr>
													<tr style='border-bottom:1px solid #d0d0d0; height:40px; font-size:18px; font-weight:bold;'>
														<td>Email :</td> <td>{$viewuser[$i]['email']} </td>
													</tr>
													<tr style='border-bottom:1px solid #d0d0d0; height:40px; font-size:18px; font-weight:bold;'>
														<td>Mobile :</td> <td>{$viewuser[$i]['mobile']} </td>
													</tr>
													<tr style='border-bottom:1px solid #d0d0d0; height:40px; font-size:18px; font-weight:bold;'>
														<td>Creation Date :</td> <td>{$viewuser[$i]['date']}</td>
													</tr>
					
												</tbody>
												
											</table>
										</div>";
									if($_SESSION['user']['type'] == $viewuser[$i]['type']){
										echo '<a class="btn btn-info" href="updatemember.php?userid='.$viewuser[$i]['id'].'">
											<i class="icon-edit icon-white"></i>  
											Edit                                            
										</a>';
									}
									if($_SESSION['user']['type'] < $viewuser[$i]['type']) {
										echo '<a class="btn btn-info" href="updatemember.php?userid='.$viewuser[$i]['id'].'">
											<i class="icon-edit icon-white"></i>  
											Edit                                            
										</a>
										<a class="btn btn-danger" href="?id='.$viewuser[$i]['id'].'">
											<i class="icon-trash icon-white"></i> 
											Delete
										</a>
										</td>';
										echo '</tr>';
									}
								}
							?>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
<?php
	include('footer.php'); 
?>
