<?php
include_once '../src/FavouritActor.php';

$objectEdit = new FavouritActor();
$objectEdit->dataPassToProperty($_GET);
$singledata = $objectEdit->singleView();

if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
?>
<fieldset>
    <legend>Update Favourit Actor</legend>
    <form action="objectUpdate.php" method="post">
        <label for="">Favourit actor</label>
        <input type="text" name="fActor" value="<?php echo $singledata['name'] ?>" />
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>" />
        <button type="submit">Update</button>
    </form>
</fieldset>
<h3><a href="actorList.php">Back to list</a></h3>
