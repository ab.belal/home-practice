<?php

session_start();

//print_r($_SESSION['N_Mes']);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Contact Form</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<!--contact-form-->
<div class="contact-form">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">

                <h3>Contact Form</h3>

                <?php
                if (isset($_SESSION['S_Mess']) && !empty($_SESSION['S_Mess'])){
                    echo "<h4 class='text-success'>".$_SESSION['S_Mess']."</h4>";
                    unset($_SESSION['S_Mess'], $_SESSION['Form_data']);
                } else {
                    echo $_SESSION['E_Mess'];
                    unset($_SESSION['E_Mess']);
                }
                ?>

                <form action="process.php" method="post">
                    <label>Name</label>
                    <input class="form-control" type="text" name="name" placeholder="Enter Your Name" value="<?php
                    if (isset($_SESSION['Form_data']['name']) && !empty($_SESSION['Form_data']['name'])) {
                        echo $_SESSION['Form_data']['name'];
                        unset($_SESSION['Form_data']['name']);
                    }
                    ?>">

                    <p class="text-danger marg-area"><?php
                        if (isset($_SESSION['N_Mes']) && !empty($_SESSION['N_Mes'])) {
                            echo $_SESSION['N_Mes'];
                            unset($_SESSION['N_Mes']);
                        }
                        ?>
                    </p><br>

                    <label>Email</label>
                    <input class="form-control" type="email" name="email" placeholder="Enter Your Email" value="<?php
                    if (isset($_SESSION['Form_data']['email']) && !empty($_SESSION['Form_data']['email'])) {
                        echo $_SESSION['Form_data']['email'];
                        unset($_SESSION['Form_data']['email']);
                    }
                    ?>">

                    <p class="text-danger marg-area"><?php
                        if (isset($_SESSION['E_Mes']) && !empty($_SESSION['E_Mes'])) {
                            echo $_SESSION['E_Mes'];
                            unset($_SESSION['E_Mes']);
                        }
                        ?>
                    </p><br>

                    <label>Subject</label>
                    <input class="form-control" type="text" name="subject" placeholder="Enter Your Subject" value="<?php
                    if (isset($_SESSION['Form_data']['subject']) && !empty($_SESSION['Form_data']['subject'])) {
                        echo $_SESSION['Form_data']['subject'];
                        unset($_SESSION['Form_data']['subject']);
                    }
                    ?>">

                    <p class="text-danger marg-area"><?php
                        if (isset($_SESSION['S_Mes']) && !empty($_SESSION['S_Mes'])) {
                            echo $_SESSION['S_Mes'];
                            unset($_SESSION['S_Mes']);
                        }
                        ?>
                    </p><br>

                    <label>Message</label>
                    <textarea rows="7" class="form-control" name="message"
                              placeholder="Enter Your Message"><?php
                    if (isset($_SESSION['Form_data']['message']) && !empty($_SESSION['Form_data']['message'])) {
                        echo $_SESSION['Form_data']['message'];
                        unset($_SESSION['Form_data']['message']);
                    }
                    ?></textarea>

                    <p class="text-danger marg-area"><?php
                        if (isset($_SESSION['M_Mes']) && !empty($_SESSION['M_Mes'])) {
                            echo $_SESSION['M_Mes'];
                            unset($_SESSION['M_Mes']);
                        }
                        ?>
                    </p><br>

                    <input class="btn btn-block" type="submit" value="Send">
                </form>
            </div>
        </div>
    </div>
</div>
<!--/contact-form-->
</body>
</html>