<?php
session_start();
if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
?>
<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Gender selection</title>
    </head>
    <body>
        <fieldset>
            <legend> <h1>Select your Gender</h1></legend>
            <form action="objectStore.php" method="post">
                <input type="radio" name="gender" value="male" id="male" checked /> <label for="male">Male</label><br/>
                <input type="radio" name="gender" value="female"id="female" /><label for="female">Female</label><br/>
                <button type="submit">Submit</button>
            </form>
        </fieldset>
        <h3><a href="list.php">See Data List</a></h3>
    </body>
</html>
