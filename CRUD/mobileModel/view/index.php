
<html>
    <head>
        <meta charset="UTF-8">
        <title>mobile model</title>
    </head>
    <body>
        <h1>download as<a href="pdf.php" target="_blank"> PDF</a>
        <a href="xl.php" target="_blank"> XL</a>
        </h1>
        
        <?php
        include_once '../vendor/autoload.php';
        use mobileApp\Mobilemodel;
        $obj = new Mobilemodel();
        $showList = $obj->mobileList();

        if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
        ?>
        <h1><a href="create.php">Add new model</a></h1>
        <h1><a href="../index.php">Back to home page</a></h1>
        <table border="1">
            <tr>
                <th>Id</th>
                <th>mobile_model</th>
                <th>laptop_model</th>
                <th colspan="3">Action</th>
            </tr>
            <?php
            $serial = 1;
            if (isset($showList) && !empty($showList)) {
                foreach ($showList as $singledata) {
                    ?>
                    <tr>
                        <td><?php echo $serial++ ?></td>
                        <td><?php echo $singledata['models'] ?></td>
                        <td><?php echo $singledata['laptop_model'] ?></td>
                        <td><a href="show.php?id=<?php echo $singledata['unique_id'] ?>">View</a></td>
                        <td><a href="edit.php?id=<?php echo $singledata['unique_id'] ?>">Update</a></td>
                        <td><a href="delete.php?id=<?php echo $singledata['unique_id'] ?>">Delete</a></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="3" align="center"> Durh mia....!!! Data nai </td>
                </tr>
                <?php
            }
            ?>

        </table>

    </body>
</html>