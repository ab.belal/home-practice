<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Mobile Model Show</title>
    </head>
    <body>
        <?php
        include_once '../vendor/autoload.php';
        use mobileApp\Mobilemodel;
        $obj = new Mobilemodel();
        $obj->dataPassToProperty($_GET);
        $data = $obj->singleDataShow();
        if (isset($data) && !empty($data)) {
            ?>

            <table border="1" cellpadding="10">
                <tr>
                    <th>Id</th>
                    <th>Model</th>
                    <th>laptop_model</th>
                    <th>unique id</th>
                </tr>
                <tr>
                    <td><?php echo $data['id'] ?></td>
                    <td><?php echo $data['models'] ?></td>
                    <td><?php echo $data['laptop_model'] ?></td>
                    <td><?php echo $data['unique_id'] ?></td>
                </tr>
            </table>
        <?php
        } else {
            $_SESSION['err_msg']="<h1>not found</h1>";
            header('location:error.php');
        }
        ?>
        <a href="index.php">Back to list</a>

    </body>
</html>