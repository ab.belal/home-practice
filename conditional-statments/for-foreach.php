<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>loops</title>
    </head>
    <body>
        <h1>for loop<br/>
            <?php
            for ($i = 1; $i <= 10; $i++)
                echo $i;
            ?>
        </h1>

        <h1>foreach<br/>
            <?php
            $value = array('red', 'green', 'blue');
            foreach ($value as $data) {
                echo "$data<br/>";
            }
            ?>
        </h1>

    </body>
</html>
