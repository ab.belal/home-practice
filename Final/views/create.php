<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Trainee Registration</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->


        <!-- Core JS files -->
        <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/forms/inputs/touchspin.min.js"></script>

        <script type="text/javascript" src="assets/js/core/app.js"></script>
        <script type="text/javascript" src="assets/js/pages/form_input_groups.js"></script>
        <!-- /theme JS files -->

        <!--my custom css styles-->
        <link href="styleforCreate.css" rel="stylesheet" type="text/css">

    </head>
    <?php
    include_once("../vendor/autoload.php");

use App\Project;

$obj = new Project();

    $course_info = $obj->Allcourse();
    if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
        echo $_SESSION['Message'];
        unset($_SESSION['Message']);
    }
    ?>
	
    <body>
        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt=""></a>

                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="sidebar-control sidebar-main-toggle hidden-xs">
                            <i class="icon-paragraph-justify3"></i>
                        </a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/flags/us.png" alt="">
                            <span>English</span>
                            <i class="caret"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#"><img src="assets/images/flags/us.png" alt=""><span>English</span></a></li>
                            <li><a href="#"><img src="assets/images/flags/bd.png" alt=""><span>Bangla</span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="category-content">
                                <div class="media">
                                    <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">PHP Debugger</span>
                                        <div class="text-size-mini text-muted">
                                            <i class="icon-pin text-size-small"></i> &nbsp; BITM, Dhaka
                                        </div>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list">
                                            <li>
                                                <a href="#"><i class="icon-cog3"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->


                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">



                                    <!-- Forms -->
                                    <li class="navigation-header"><span>Forms</span> <i class="icon-menu" title="Forms"></i></li>
                                    <li>
                                        <a href="#"><i class="icon-pencil3"></i> <span>Form components</span></a>
                                        <ul>
                                            <li><a href="form_inputs_basic.html">Basic inputs</a></li>
                                            <li><a href="form_checkboxes_radios.html">Checkboxes &amp; radios</a></li>
                                            <li class="active"><a href="form_input_groups.html">Input groups</a></li>
                                            <li><a href="form_controls_extended.html">Extended controls</a></li>
                                            <li>
                                                <a href="#">Selects</a>
                                                <ul>
                                                    <li><a href="form_select2.html">Select2 selects</a></li>
                                                    <li><a href="form_multiselect.html">Bootstrap multiselect</a></li>
                                                    <li><a href="form_select_box_it.html">SelectBoxIt selects</a></li>
                                                    <li><a href="form_bootstrap_select.html">Bootstrap selects</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="form_tag_inputs.html">Tag inputs</a></li>
                                            <li><a href="form_dual_listboxes.html">Dual Listboxes</a></li>
                                            <li><a href="form_editable.html">Editable forms</a></li>
                                            <li><a href="form_validation.html">Validation</a></li>
                                            <li><a href="form_inputs_grid.html">Inputs grid</a></li>
                                        </ul>
                                    </li>

                                    <!-- /forms -->



                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->

                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Forms</span> Registration</h4>
                            </div>
                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                                <li class="active">Registration</li>
                            </ul>


                        </div>
                    </div>
                    <!-- /page header -->




                    <div class="formContent">

                        <legend>Registration form</legend>
                        <form action="store.php" method="post" enctype="multipart/form-data">
                            <div class="forms fullName">
                                <label>Enter full name</label>
                                <input type="text" name="full_name" placeholder="Enter Name" value="<?php
								if (isset($_SESSION['Form']['full_name'])) {
									echo $_SESSION['Form']['full_name'];
									unset($_SESSION['Form']['full_name']);
								}
								?>">

                                <p class="error">
                                    <?php
                                    if (!empty($_SESSION['fnemty'])) {
                                        echo $_SESSION['fnemty'];
                                        unset($_SESSION['fnemty']);
                                    }
                                    ?>
                                </p>   
                            </div>


                            <div class="forms education">
                                <label>Educational Title</label>
                                <input type="text" name="title" placeholder="Enter Educational Title" value="<?php
                                    if (isset($_SESSION['Form']['title'])) {
                                        echo $_SESSION['Form']['title'];
                                        unset($_SESSION['Form']['title']);
                                    }
                                    ?>"> 

                                <p class="error">
                                    <?php
                                    if (!empty($_SESSION['temty'])) {
                                        echo $_SESSION['temty'];
                                        unset($_SESSION['temty']);
                                    }
                                    ?>
                                </p>
                            </div>

                            <div class="forms institute">
                                <label>Institution Name</label>
                                <input type="text" name="institute" placeholder="Enter Institute name"><br>
                            </div>

                            <div class="forms passYear">
                                <label>Passing year</label>
                                <input type="text" name="year" placeholder="Enter Passing Year"></fieldset><br>
                            </div>

                            <div class="forms team">
                                <label>Team name</label>
                                <select name="team" >
                                    <option value="">Select Team</option>
                                    <option value="Team1">Team 1</option>
                                    <option value="Team2">Team 2</option>
                                    <option value="Team3">Team 3</option>
                                    <option value="Team4">Team 4</option>
                                    <option value="Team5">Team 5</option>
                                    <?php ?><?php ?>
                                </select>
                            </div>

                            <div class="forms course">
                                <label>Course Name</label>
                                <select name="courses_id">
                                    <?php
                                    foreach ($course_info as $oneinfo) {
                                        ?>
                                        <option value="<?php echo $oneinfo['id'] ?>"><?php echo $oneinfo['title']; ?></option>
                                    <?Php }
                                    ?>

                                </select>
                            </div>

                            <div class="forms trainer">
                                <label>Trainer Status</label>
                                <select name="trainer_status">
                                    <option value="0">Select Status</option>
                                    <option value="leadtrainer">Lead Trainer</option>
                                    <option value="assttrainer">Assistant Trainer</option>
                                    <option value="labasst">Lab Assistant</option>
                                </select>
                            </div>

                            <div class="forms phone">
                                <label>Phone</label>
                                <input type="text" name="phone" placeholder="Enter Phone" value="<?php
                                    if (isset($_SESSION['Form']['phone'])) {
                                        echo $_SESSION['Form']['phone'];
                                        unset($_SESSION['Form']['phone']);
                                    }
                                    ?>">
                                <p class="error">
                                    <?php
                                    if (!empty($_SESSION['pemty'])) {
                                        echo $_SESSION['pemty'];
                                        unset($_SESSION['pemty']);
                                    }
                                    ?>
                                </p>

                            </div>

                            <div class="forms email">
                                <label>Email</label>
                                <input type="text" name="email" placeholder="Enter Email" value="<?php
                                    if (isset($_SESSION['Form']['email'])) {
                                        echo $_SESSION['Form']['email'];
                                        unset($_SESSION['Form']['email']);
                                    }
                                    ?>">

                                <p class="error">
                                    <?php
                                    if (!empty($_SESSION['eemty'])) {
                                        echo $_SESSION['eemty'];
                                        unset($_SESSION['eemty']);
                                    }
                                    ?>
                                </p>
                            </div>


                            <div class="forms address1">
                                <label>Address line 1</label>
                                <input type="text" name="add1" placeholder="Enter Address" value="<?php
                                    if (isset($_SESSION['Form']['a1emty'])) {
                                        echo $_SESSION['Form']['a1emty'];
                                        unset($_SESSION['Form']['a1emty']);
                                    }
                                    ?>">

                                <p class="error">
                                    <?php
                                    if (!empty($_SESSION['a1emty'])) {
                                        echo $_SESSION['a1emty'];
                                        unset($_SESSION['a1emty']);
                                    }
                                    ?>
                                </p>
                            </div>

                            <div class="forms address2">
                                <label>Address line 2</label>
                                <input type="text" name="add2" placeholder="Enter Address" value="<?php
                                    if (isset($_SESSION['Form']['a2emty'])) {
                                        echo $_SESSION['Form']['a2emty'];
                                        unset($_SESSION['Form']['a2emty']);
                                    }
                                    ?>">

                                <p class="error">
                                    <?php
                                    if (!empty($_SESSION['a2emty'])) {
                                        echo $_SESSION['a2emty'];
                                        unset($_SESSION['a2emty']);
                                    }
                                    ?>
                                </p>
                            </div>

                            <div class="forms city">
                                <label>City</label>
                                <input type="text" name="city" placeholder="Enter City" value="<?php
                                    if (isset($_SESSION['Form']['a3emty'])) {
                                        echo $_SESSION['Form']['a3emty'];
                                        unset($_SESSION['Form']['a3emty']);
                                    }
                                    ?>">
                                <p class="error">
                                    <?php
                                    if (!empty($_SESSION['a3emty'])) {
                                        echo $_SESSION['a3emty'];
                                        unset($_SESSION['a3emty']);
                                    }
                                    ?>
                                </p>
                            </div>

                            <div class="forms zip">
                                <label>Zip</label>
                                <input type="text" name="zip" placeholder="Enter Zip Code" value="<?php
                                    if (isset($_SESSION['Form']['a4emty'])) {
                                        echo $_SESSION['Form']['a4emty'];
                                        unset($_SESSION['Form']['a4emty']);
                                    }
                                    ?>">
                                <p class="error">
                                    <?php
                                    if (!empty($_SESSION['a4emty'])) {
                                        echo $_SESSION['a4emty'];
                                        unset($_SESSION['a4emty']);
                                    }
                                    ?>
                                </p>

                            </div>

                            <div class="forms gender">
                                <label>Select gender</label>
                                <input type="radio" name="gender" value="Male ">Male
                                <input type="radio" name="gender" value="Female">Female
                            </div>

                            <div class="forms web">
                                <label>Web Address</label>
                                <input type="text" name="web" placeholder="Enter Web Address" value="">
                            </div>

                            <div class="forms img">
                                <label>Upload picture</label>
                                <img src="<?php echo "images/" . $mydata['image'] ?>">
                                <input type="file" name="image"/>
                                <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>"/>
                            </div>
                            <div style="clear:both;"></div>
                            <input type="submit" value="Sign Up">
                        </form>
                        <div class="footer text-muted">
                            &copy; 2015. <a href="#">Trainer Apps</a> by <a target="_blank" href="#">PHP Debugger</a>
                        </div>
                    </div>
                </div>
                <!-- /content wrapper -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
    </body>
</html>