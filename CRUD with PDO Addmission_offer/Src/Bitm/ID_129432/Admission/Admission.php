<?php

/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 7/19/2016
 * Time: 10:07 AM
 */
namespace App\Bitm\ID_129432\Admission;
use PDO;
class Admission
{
    public $id="";
    public $name="";
    public $semester="";
    public $offer="";
    public $cost="";
    //public $sem1="";
   // public $sem2="";
   // public $sem3="";
    public $weber="";
    public $total="";
    public $data="";

    public $db_user="root";
    public $db_pass="";
    public $coon="";





    public function __construct()
    {
        session_start();
        $this->coon=new PDO('mysql:host=localhost;dbname=phpadd', $this->db_user, $this->db_pass);
            //$conn = mysql_connect('localhost', 'root', '') or die('Opps! Unable to connect with Mysql');
        //mysql_select_db('phpadd') or die('Unable to connect with Database');
    }

    public function prepare($data=""){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(!empty($data['your_name'])){
            $this->name=$data['your_name'];
        }else{
            $_SESSION['name_masg']="This field must be require";
        }
        if(!empty($data['semester'])){
            $this->semester=$data['semester'];
        }else{
            $_SESSION['semester_masg']="This field must be require";
        }
        if(array_key_exists('get_offer',$data)){
            $this->offer=$data['get_offer'];
        }
        if(array_key_exists('cost',$data)){
            $this->cost=$_POST['cost'];
        }
        if(array_key_exists('wever',$data)){
            $this->weber=$_POST['wever'];
        }
        if(array_key_exists('total',$data)){
            $this->total=$_POST['total'];
        }

        $_SESSION['formdata']=$data;



        //$this->sem1=$data[$_POST['first']];
        //$this->sem2=$data[$_POST['second']];
        //$this->sem3=$data[$_POST['third']];

//        echo "<pre>";
//        print_r($data);
        return $this;
    }

    public function store(){
        try{
            $query="INSERT INTO admission(id,name,semester,offer,cost,wever,total,unique_id)
            VALUES (:i,:n,:s,:o,:c,:w,:t,:u)";
            $stmt=$this->coon->prepare($query);
            $stmt->execute(array(
                ':i'=>NULL,
                ':n'=>$this->name,
                ':s'=>$this->semester,
                ':o'=>$this->offer,
                ':c'=>$this->cost,
                ':w'=>$this->weber,
                ':t'=>$this->total,
                ':u'=>uniqid(),
            ));
            header('location:index.php');
//
        }catch (PDOException $e){
            echo 'Error: '. $e->getMessage();
        }
//        if(!empty($this->name && !empty($this->semester))){
//            $query="INSERT INTO `phpadd`.`admission` (`id`, `name`, `semester`, `offer`, `cost`, `wever`, `total`,`unique_id`)
//        VALUES (NULL, '$this->name', '$this->semester', '$this->offer', '$this->cost', '$this->weber', '$this->total','".uniqid()."')";
//            if(mysql_query($query)){
//                $_SESSION['massage']="Data sucessfully added";
//                header('location:create.php');
//            }
//        }else{
//            header('location:create.php');
//        }
                //echo $query;


    }

    public function index(){
        $sql = "SELECT * FROM `admission`";
        $query=$this->coon->prepare($sql);
        $query->execute();
        while($result=$query->fetch(PDO::FETCH_ASSOC)){
            $this->data[]=$result;
        }
        return $this->data;
//        $query = "SELECT * FROM `admission`";
//        $mydata = mysql_query($query);
//        while ($row = mysql_fetch_assoc($mydata)) {
//            $this->data[] = $row;
//        }
//        return $this->data;
    }

    public function show(){
        $sql="SELECT * FROM `admission` WHERE `unique_id`="."'".$this->id."'";
        $query=$this->coon->prepare($sql);
        $query->execute();
        $row=$query->fetch(PDO::FETCH_ASSOC);
        return $row;

        //$query="SELECT * FROM `admission` WHERE `unique_id`="."'".$this->id."'";
        //echo $query;
//die();
//        $mydata=mysql_query($query);
//        $row = mysql_fetch_assoc($mydata);
//            return $row;

    }

    public function update(){
        $sql="UPDATE `admission` SET `name` = '$this->name', `semester` = ' $this->semester', `offer` = '$this->offer', `cost` = '$this->cost', `wever` = '$this->weber', `total` = '$this->total' WHERE `admission`.`unique_id` ="." '".$this->id."'";
        $query=$this->coon->prepare($sql);
        $query->execute();
        header('location:index.php');
//        $query="UPDATE `admission` SET `name` = '$this->name', `semester` = ' $this->semester', `offer` = '$this->offer', `cost` = '$this->cost', `wever` = '$this->weber', `total` = '$this->total' WHERE `admission`.`unique_id` ="." '".$this->id."'";
//        //echo $query;
//        if(mysql_query($query)){
//            $_SESSION['massage']="Data sucessfully Updated";
//            header('location:index.php');
//        }

    }
    
    
    public function delete(){
        $sql="DELETE FROM `admission` WHERE `admission`.`unique_id` ="." '".$this->id."'";
        $query=$this->coon->prepare($sql);
        $query->execute();
        header('location:index.php');
//        $query="DELETE FROM `admission` WHERE `admission`.`unique_id` ="." '".$this->id."'";
//        //echo $query;
//        if(mysql_query($query)){
//            $_SESSION['massage']="Data sucessfully deleted";
//            header('location:index.php');
//        }
    }


}