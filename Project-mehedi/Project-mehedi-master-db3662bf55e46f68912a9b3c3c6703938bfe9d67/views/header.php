<?php

require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_registration\user_registration;

$objSingleUser = new user_registration();
//$_POST['id'] = $_SESSION['logged']['unique_id'];

$objSingleUser -> prepare($_POST);

$singleUserLoggin = $objSingleUser -> single_user();

    function module_name(){
        $str_replace = str_replace("/project-mehedi/views/","",$_SERVER['REQUEST_URI']);
            $exploded =  explode('/', $str_replace);
            if(!empty($exploded)){
                return ucfirst($exploded[0]); 
            }

        }

    function checked($value = "", $key = ""){ // to hold select point of dropdown
        if(isset($_SESSION["$value"])){
            if($_SESSION["$value"] == "$key"){
                echo 'selected="selected"';
                unset($_SESSION["$value"]);
            }else{
                echo '';
            }
        }
    }           
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo module_name(); ?></title>
        <link rel='shortcut icon' href='../assets/images/favicon.png' type='image'>

        <!-- Global stylesheets -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"> -->
        <link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Our custom CSS -->
        <link href="../assets/css/custom.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/timepicker.less" rel="stylesheet" type="text/css">
        <link href="../assets/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css">
        <!-- /Our custom CSS -->

        <!-- Core JS files -->
        <script src="../assets/js/plugins/loaders/pace.min.js" type="text/javascript"></script>
        <script src="../assets/js/plugins/loaders/blockui.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="../assets/js/pages/form_bootstrap_select.js" type="text/javascript"></script>

        <script src="../assets/js/plugins/forms/selects/bootstrap_select.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../assets/js/core/app.js"></script>
        <!-- /theme JS files -->

        <!-- Pagination JS files -->
        <script type="text/javascript" src="../assets/js/core/pagination/footable.min.js"></script>
        <script type="text/javascript" src="../assets/js/core/pagination/footable.paginate.min.js"></script>
        <!-- /Pagination JS files -->

        <!-- Appear textbox after select paid course -->
        
        <!-- /Appear textbox after select paid course -->
        <!-- course disable confirmation js-->

        <script type="text/javascript" src="../assets/js/plugins/timepicker/bootstrap-timepicker.js"></script>

        <script type="text/javascript" src="../assets/js/plugins/timepicker/bootstrap-datepicker.min.js"></script>
        
    </head>

    <body class="sidebar-xs">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html"><img src="../assets/images/logo_light.png" alt=""></a>

            <ul class="nav navbar-nav pull-right visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            </ul>
        </div>


        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">

                <li class="<?php echo module_name()=='Assign'? 'active':'';?>"><a href="../assign/index.php"><i class="icon-home5 position-left"></i> Assign Session</a></li>

                <li class="<?php echo module_name()=='Users'? 'active':'';?>"><a href="../users/index.php"><i class="icon-users2 position-left"></i> User</a></li>
                                
                <li class="<?php echo module_name()=='Trainers'? 'active':'';?>"><a href="../trainers/index.php"><i class="icon-people position-left"></i> Trainer</a></li>
                                            
                <li class="<?php echo module_name()=='Courses'? 'active':'';?>"><a href="../courses/index.php"><i class=" icon-graduation2 position-left"></i> Course</a></li>
                                                
                <li class="<?php echo module_name()=='Labinfo'? 'active':'';?>"><a href="../labinfo/index.php"><i class="icon-display position-left"></i> Labinfo</a></li>
                                                    
                <li class="<?php echo module_name()=='Softwar'? 'active':'';?>"><a href="../software/index.php"><i class="icon-gear position-left"></i> Software & Lab Mapping</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="../assets/images/user/<?php echo $singleUserLoggin['image'];?>" alt="">
                        <span><?php echo $singleUserLoggin['username'];?></span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="../users/single_user.php?id=<?php echo $singleUserLoggin['unique_id'];?>"><i class="icon-user-plus"></i> My profile</a></li>
                        <li><a href="../users/logout_user.php"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
    <!-- /main navbar -->



    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <div class="sidebar-user">
                        <div class="category-content">
                            <div class="media">
                                <a href="../users/single_user.php?id=<?php echo $singleUserLoggin['unique_id'];?>" class="media-left"><img src="../assets/images/user/<?php echo $singleUserLoggin['image'];?>" class="img-circle img-sm" alt=""></a>
                                <div class="media-body">
                                    <span class="media-heading text-semibold"><?php echo $singleUserLoggin['full_name'];?></span>
                                    <div class="text-size-mini text-muted">
                                     &nbsp;<?php echo ($singleUserLoggin['is_admin'] == 1)?'Admin':'User';?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /user menu -->

                    <!-- Main navigation -->
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">

                            <!-- Main -->
                                <li class="navigation-header"><span>Main Module</span> <i class="icon-menu" title="Main pages"></i></li>
                                <li class="<?php echo module_name()=='Assign'? 'active':'';?>"><a href="../assign/index.php"><i class="icon-home5 position-left"></i> Assign Session</a></li>

                                <li class="<?php echo module_name()=='Users'? 'active':'';?>"><a href="../users/index.php"><i class="icon-users2 position-left"></i> User</a></li>
                                
                                <li class="<?php echo module_name()=='Trainers'? 'active':'';?>"><a href="../trainers/index.php"><i class="icon-people position-left"></i> Trainer</a></li>
                                            
                                <li class="<?php echo module_name()=='Courses'? 'active':'';?>"><a href="../courses/index.php"><i class=" icon-graduation2 position-left"></i> Course</a></li>
                                                
                                <li class="<?php echo module_name()=='Labinfo'? 'active':'';?>"><a href="../labinfo/index.php"><i class="icon-display position-left"></i> Labinfo</a></li>
                                                    
                                <li class="<?php echo module_name()=='Softwar'? 'active':'';?>"><a href="../software/index.php"><i class="icon-gear position-left"></i> Software & Lab Mapping</a></li>
                            <!-- /main -->

                            </ul>
                        </div>
                    </div>
                    <!-- /main navigation -->

                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-users2 position-left"></i> <span class="text-semibold"><?php echo module_name(); ?></span> Module</h4>
                        </div>

                        <div class="heading-elements">
                            <form action="../assign/search.php" method="GET" id="form_search">
                                <div class="input-group">
                                    <input class="form-control border-slate border-lg" type="text" name="searchText" placeholder="Search here..">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default border-slate dropdown-toggle btn-icon" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>

                                        <select class="dropdown-menu dropdown-menu-right" name="search_field">
                                            <option value="full">Search in Running Session</option>
                                            <option value="course">Search in Course</option>
                                            <option value="trainer">Search in Trainer</option>
                                        </select>

                                        <button class="btn bg-slate-700" type="submit" form="form_search" value="Submit">
                                            <i class="icon-search4"></i>
                                        </button>
                                    </span>

                                </div>
                            </form>
                        </div>
                    </div>

                    
                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><i class="icon-home2 position-left"></i> Home</li>
                            <?php
                                $str_replace = str_replace("/project-mehedi/views/","",$_SERVER['REQUEST_URI']);
                                $exploded =  explode('/', $str_replace);
                                
                                foreach( $exploded as $path){

                                    if(strpos($path, '.php')){
                                        $substr = substr($path, strpos($path, ".php"));
                                        $trimdata = rtrim($path, $substr);
                                        echo "<li>".ucfirst($trimdata)."</li>";
                                    }else{
                                        echo "<li>".ucfirst(basename($path,'.php'))."</li>";
                                    }

                                }
                            ?>
                        </ul>