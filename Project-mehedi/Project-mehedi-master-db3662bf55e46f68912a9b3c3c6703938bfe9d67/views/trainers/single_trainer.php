<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\trainers\trainers;
$objLoginUser = new user_login();
$objLoginUser -> login_check();


$objSingleTrainers = new trainers();

$objSingleTrainers -> prepare($_GET);

$singleTrainer = $objSingleTrainers -> single_Trainer();

if(!empty($singleTrainer) && isset($singleTrainer)){

include_once '../header.php';
include_once 'menubar.php';

?>
<div class="row">
	<div class="col-lg-4 col-sm-6">
		<div class="thumbnail">
			<div class="thumb thumb-slide">
				<img alt="" src="
				<?php
					if(isset($singleTrainer['image']) && !empty($singleTrainer['image'])){
						echo '../assets/images/trainer/'.$singleTrainer['image'];
					}else{
						echo '../assets/images/trainer/nobody.jpg';
					}
				 ?>">
				<!--
				<div class="caption">
					<span>
						<a data-popup="lightbox" class="btn bg-success-400 btn-icon btn-xs" href="#"><i class="icon-plus2"></i></a>
					</span>
				</div>
				-->
			</div>
			
			<div class="caption text-center">
				<h6 class="text-semibold no-margin"><?php echo $singleTrainer['full_name'];?> <small class="display-block">
				<?php
				 	if($singleTrainer['trainer_status']=='lead_trainer'){
				 		echo "Lead Trainer in BITM";
				 	}elseif($singleTrainer['trainer_status'] == 'assist_trainer'){
				 		echo "Assistant Trainer in BITM";
				 	}else{
				 		echo "Lab Assistant in BITM";
				 	}
				 ?></small></h6>
			</div>
		</div>
	</div>
	<div class="col-lg-8 col-sm-6">
		<div class="panel border-left-lg border-left-primary invoice-grid timeline-content">
			
			<div class="pane-heading text-right">
				<div class="btn-group text-right">
					<a href="edit_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>" class="btn btn-default" type="button"><i class="icon-pencil7 position-left"></i> Edit Trainer</a>
					<?php 
                        if($_SESSION['logged']['is_admin'] == 1){
                    ?>
					<a href="delete_trainer.php?id=<?php echo $singleTrainer['unique_id'];?>" class="btn btn-default" type="button" onclick="return confirm('Are you sure you want to disable this Trainer?');"><i class="icon-close2 position-left"></i> Block Trainer</a>
					<?php
						}
					?>
				</div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-8 col-md-8">
						<h3 class="text-semibold no-margin-top"><?php echo $singleTrainer['full_name'];?></h3>
						<ul class="list list-unstyled">
							<li>Assigned for <?php echo $singleTrainer['title'];?></li>
							<li>
								<?php 
									$eduction = unserialize($singleTrainer['edu_status']);
								?>
								<?php
									echo $eduction[0];
									if(isset($eduction[1])){
										echo ", ".$eduction[1];
									}
									if(isset($eduction[2])){
										echo ", Passed at ".$eduction[2];
									}
								?>
							</li>

							<li>Phone: <span class="text-semibold">
								<?php echo $singleTrainer['phone'];?></span>
							</li>
							<li>Email: <span class="text-semibold">
								<a href="mailto:<?php echo $singleTrainer['email'];?>"><?php echo $singleTrainer['email'];?></a></span>
							</li>
							<li>
								<?php 
									$address = unserialize($singleTrainer['address']);
								?>
								<?php
									echo $address[0];
									if(isset($address[1])){
										echo ", ".$address[1];
									}
									if(isset($address[2])){
										echo ", Zipcode: ".$address[2];
									}
								?>
							</li>
						</ul>
					</div>
					<div class="col-sm-4 col-md-4">
						<ul class="list list-unstyled text-right">
							<li>
								<h5><span class="label bg-success-400">
									<?php
									if($singleTrainer['trainer_status']=='lead_trainer'){
										echo "Lead Trainer in BITM";
									}elseif($singleTrainer['trainer_status'] == 'assist_trainer'){
										echo "Assistant Trainer in BITM";
									}else{
										echo "Lab Assistant in BITM";
										}
									?>
								</span></h5>
							</li>
							<li><span class="label bg-indigo-400"><?php echo $singleTrainer['team']; ?> TEAM</span></li>
							
						</ul>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<ul class="list list-unstyled">
					<li><span class="status-mark border-danger position-left"></span> Created Date: <span class="text-semibold">
				<?php echo $singleTrainer['created'];?></span></li>
				<li><span class="status-mark border-danger position-left"></span> Last Modified : <span class="text-semibold">
			<?php echo ($singleTrainer['updated']=='0000-00-00 00:00:00')?'Not Yet Modified': $singleTrainer['updated'];?></span></li>
			<li><span class="status-mark border-danger position-left"></span> Deleted Date: <span class="text-semibold">
		<?php echo ($singleTrainer['deleted']=='0000-00-00 00:00:00')?'Not Yet Deleted': $singleTrainer['deleted'];?></span></li>
		
	</ul>
</div>
</div>
</div>
</div>
<?php include_once 'footer.php';


}else{

	$_SESSION["errorMsg"] = "Not found what you looking for.";
	header("location:error.php");
}





 ?>
