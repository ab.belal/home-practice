<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\assign\Assign;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objAssignReset = new Assign();

$objAssignReset -> prepare($_GET);


if(isset($_GET['resetId']) && !empty($_GET['resetId'])){

	unset($_SESSION['course_id']);
	unset($_SESSION['lead_trainer']);
	unset($_SESSION['lab_asst']);
	unset($_SESSION['asst_trainer']);
	unset($_SESSION['start_date']);
	unset($_SESSION['ending_date']);
	unset($_SESSION['start_time']);
	unset($_SESSION['batch_no']);
	unset($_SESSION['lab_id']);
	unset($_SESSION['day']);
	unset($_SESSION['ending_time']);

	header("location:edit_assign.php?id=".$_GET['resetId']);
}else{
	unset($_SESSION['course_id']);
	unset($_SESSION['lead_trainer']);
	unset($_SESSION['lab_asst']);
	unset($_SESSION['asst_trainer']);
	unset($_SESSION['start_date']);
	unset($_SESSION['ending_date']);
	unset($_SESSION['start_time']);
	unset($_SESSION['batch_no']);
	unset($_SESSION['lab_id']);
	unset($_SESSION['day']);
	unset($_SESSION['ending_time']);
	
	header("location:index.php");
}
