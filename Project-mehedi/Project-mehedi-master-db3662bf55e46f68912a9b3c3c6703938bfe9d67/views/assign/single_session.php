<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\assign\Assign;
$objLoginUser = new user_login();
$objLoginUser -> login_check();
$objSingleAssign = new Assign();

$objSingleAssign->prepare($_GET);

$singleAssign = $objSingleAssign->single_assign_show();

$allTrainers = $objSingleAssign->trainer_name_status_team();

    include_once '../header.php';
    include_once 'menubar.php';

?>
            <!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4 class="text-center"><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Details of</span> <?php echo $singleAssign['title']; ?> Course</h4>
        </div>
    </div>
</div>
<div class="content-detached">
    <div class="panel panel-flat">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <div class="heading-elements">
                            <div class="heading-btn-group">
                                <a type="button" class="btn btn-default" href="edit_assign.php?id=<?php echo $singleAssign['id']; ?>"><b><i class="icon-pencil7"></i></b> Edit Course</a>
                                <?php
                                    if($_SESSION['logged']['is_admin'] == 1){
                                ?>
                                <a href="trash.php?id=<?php echo $singleAssign['id']; ?>" class="btn btn-default" onclick="return confirm('Are you sure you want to disable this course session?');"><i class="icon-close2 position-left"></i>Delete</a>
                                <?php 
                                    }
                                ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr class="success">
                                        <th class = "col-md-3"><b>ID</b></th>
                                        <td class = "col-md-9"><?php echo $singleAssign['id']; ?></td>
                                    </tr>
                                    <tr class="info">
                                        <th class = "col-md-3"><b>Course Title</b></th>
                                        <td class = "col-md-9"><?php echo $singleAssign['title']; ?></td>
                                    </tr>
                                    <tr class="success">
                                        <th class = "col-md-3"><b>Batch No</b></th>
                                        <td class = "col-md-9"><?php echo $singleAssign['batch_no']; ?></td>
                                    </tr>
                                    <tr class="info">
                                        <th class="col-md-3"><b>Lead Trainer</b></th>
                                        <?php

                                        foreach($allTrainers as $trainer){
                                          if($singleAssign['lead_trainer'] == $trainer['id']){
                                            echo '<td class = "col-md=9">'.$trainer['full_name'].'</td>';
                                          }
                                        }// foreach
                                        ?>
                                    </tr>
                                    <tr class="success">
                                        <th class = "col-md-3"><b>Asst. Trainer</b></th>
                                        <?php 
                                        foreach($allTrainers as $trainer){
                                            if($singleAssign['asst_trainer'] == $trainer['id']){
                                            echo '<td class = "col-md=9">'.$trainer['full_name'].'</td>';
                                          }
                                        }// foreach
                                        ?>
                                    </tr>
                                    <tr class="info">
                                        <th class="col-md-3"><b>Lab Asst.</b></th>
                                        <?php 
                                        foreach($allTrainers as $trainer){
                                            if($singleAssign['lab_asst'] == $trainer['id']){
                                            echo '<td class = "col-md=9">'.$trainer['full_name'].'</td>';
                                          }
                                        }// foreach
                                        ?>
                                    </tr>
                                    <tr class="success">
                                        <th class = "col-md-3"><b>Lab No</b></th>
                                        <td class = "col-md-9"><?php echo $singleAssign['lab_no']; ?></td>
                                    </tr>
                                    <tr class="info">
                                        <th class="col-md-3"><b>Start Date</b></th>
                                        <td class="col-md-9"><?php echo date('l jS F Y',strtotime($singleAssign['start_date'])); ?></td>
                                    </tr>
                                    <tr class="success">
                                        <th class = "col-md-3"><b>Ending Date</b></th>
                                        <td class="col-md-9"><?php echo date('l jS F Y',strtotime($singleAssign['ending_date']));?></td>
                                    </tr>
                                    <tr class="info">
                                        <th class="col-md-3"><b>Start Time</b></th>
                                        <td class="col-md-9"><?php echo date('h:i A',strtotime($singleAssign['start_time'])); ?> - <?php echo date('h:i A',strtotime($singleAssign['ending_time']));  ?></td>
                                    </tr>
                                    <tr class="success">
                                        <th class = "col-md-3"><b>Assigned By</b></th>
                                        <td class = "col-md-9"><?php echo $singleAssign['full_name']; ?></td>
                                    </tr>
                                    <tr class="info">
                                        <th class="col-md-3"><b>Course Running</b></th>
                                        <td class="col-md-9">
                                            <?php
                                            if ($singleAssign['is_running'] == '1') {
                                            echo "Yes";
                                            } else {
                                            echo "No";
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr class="success">
                                        <th class = "col-md-3"><b>Course Fee</b></th>
                                        <td class = "col-md-9">
                                        <?php 
                                            if($singleAssign['course_fee'] == 0){
                                                echo "Free Course. Not applicable";
                                            }else{
                                                echo $singleAssign['course_fee'];
                                            }
                                        ?>     
                                        </td>
                                    </tr>
                                    <tr class="info">
                                        <th class = "col-md-3"><b>Created</b></th>
                                        <td class = "col-md-9"><?php echo $singleAssign['created']; ?></td>
                                    </tr>
                                    <tr class="success">
                                        <th class = "col-md-3"><b>Modified</b></th>
                                        <td class = "col-md-9">
                                            <?php
                                            if ($singleAssign['updated'] == '0000-00-00 00:00:00') {
                                            echo "No Modify Yet";
                                            } else {
                                            echo $singleAssign['updated'];
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr class="info">
                                        <th class = "col-md-3"><b>Delete</b></th>
                                        <td class = "col-md-9">
                                            <?php
                                            if ($singleAssign['deleted'] == '0000-00-00 00:00:00') {
                                            echo "No Delete Yet";
                                            } else {
                                            echo $singleAssign['deleted'];
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr class="success">
                                        <th class = "col-md-3"><b>Course Description</b></th>
                                        <td class = "col-md-9"><?php echo $singleAssign['description']; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- row -->
    </div>
</div>

<?php include_once 'footer.php'; ?>
