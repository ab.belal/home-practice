<?php
            function activeClassNav($path=""){

               if(basename($_SERVER['REQUEST_URI'],'.php') == $path){
                   echo 'active';
               }
        }
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            <?php
                    
                    $filename = (pathinfo(basename($_SERVER['REQUEST_URI'],'.php')));
                    echo $filename['filename'];
                    // Autometic change Title as file name ... -Mehedi
            ?>
        </title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>

        <script type="text/javascript" src="../assets/js/core/app.js"></script>
        <script type="text/javascript" src="../assets/js/pages/form_layouts.js"></script>
        <!-- course disable confirmation js-->
        <script language="JavaScript" type="text/javascript">
        function checkDelete(){
            return confirm('Are you sure to disable this course?');
        }
        </script>

    </head>

    <body class="sidebar-xs has-detached-left">

        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html"><img src="../assets/images/logo_test.png" alt=""></a>

                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a class="sidebar-mobile-detached-toggle"><i class="icon-grid7"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../assets/images/image.png" alt="">
                            <span><?php echo "UserName"; ?></span>
                            <i class="caret"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <!--<div class="page-container">-->

            <!-- Page content -->
            <!--<div class="page-content">-->

                <!-- Main content -->
                <div class="content-wrapper">
                    <div class="navbar navbar-default navbar-xs">
                        <ul class="nav navbar-nav no-border visible-xs-block">
                            <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second"><i class="icon-circle-down2"></i></a></li>
                        </ul>

                        <div class="navbar-collapse collapse" id="navbar-second">
                            <ul class="nav navbar-nav navbar-right">
                                
                                <li class="<?php activeClassNav('index');?>"><a href="index.php">
                                        <i class=" icon-new position-left">
                                        </i> Assign New Session</a>
                                </li>
                                <li class=""><a href="list.php">
                                        <i class=" icon-list-unordered position-left">
                                        </i> All Session</a>
                                </li>
                                <li class="<?php activeClassNav('create');?>"><a href="../create.php">
                                        <i class=" icon-new-tab2 position-left">
                                        </i> Create Course</a>
                                </li>
                                <li class="<?php activeClassNav('index');?>"><a href="../index.php">
                                        <i class=" icon-list position-left">
                                        </i> Course List</a>
                                </li>
                                <li class="<?php activeClassNav('free_course');?>"><a href="../freecourse.php">
                                        <i class="icon-chevron-left position-left">
                                        </i> Free Course</a>
                                </li>
                                
                                <li class="<?php activeClassNav('free_course');?>"><a href="../paidcourse.php">
                                        <i class="icon-coin-dollar position-left">
                                        </i> Paid Course</a>
                                
                                <li class="<?php activeClassNav('offered_course');?>"><a href="../offeredcourse.php">
                                        <i class="icon-star-full2 position-left">
                                        </i> Offered Course</a>
                                </li>
                                
                                <li class="<?php activeClassNav('trashlist');?>"><a href="../trashlist.php">
                                        <i class="icon-blocked position-left">
                                        </i> Disabled Course</a>
                                </li>
                                
                            </ul>

                        </div>
                    </div>
                    
                    <!-- /second navbar -->
