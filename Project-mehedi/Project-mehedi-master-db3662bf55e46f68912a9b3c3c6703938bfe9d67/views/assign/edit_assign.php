<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\assign\Assign;
$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objAssignEdit = new Assign();

$objAssignEdit->prepare($_GET);

$singleAssign = $objAssignEdit->single_assign_show();

// print_r($singleAssign);

//
$allTrainers = $objAssignEdit->trainer_name_status_team();
//
$allLabs = $objAssignEdit->lab_id_labno();

$allCourses = $objAssignEdit->course_id_title();


    function errMsg($data = ""){ // Show the Error session msg from class file
        if(isset($_SESSION["$data"])){
        ?>
        <label class="validation-error-label text-left">
            <?php
            if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
            echo $_SESSION["$data"]."</label>";
            unset($_SESSION["$data"]);
            }
        }
    }

    function errMsgSuc($data = ""){ // Show the Error session msg in Green color
        if(isset($_SESSION["$data"])){
        ?>
            <label class="validation-error-label text-success text-left">
        <?php
            if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
                echo '<b>'.$_SESSION["$data"]."<b></label>";
                unset($_SESSION["$data"]);
            }
        }
    }


    function session_DB_value_update($session_data = ""){ 
        global $singleAssign;
        if(isset($_SESSION["$session_data"]) && !empty($_SESSION["$session_data"])){
                echo $_SESSION["$session_data"];
                unset($_SESSION["$session_data"]);
        }else{
            echo $singleAssign["$session_data"];
        }
    }

    function session_DB_startTime_endTime($session_data = ""){ 
        global $singleAssign;
        if(isset($_SESSION["$session_data"]) && !empty($_SESSION["$session_data"])){
                echo $_SESSION["$session_data"];
                unset($_SESSION["$session_data"]);
        }else{
            echo date('h:i A',strtotime($singleAssign["$session_data"]));
        }
    }

    function session_checked($value = "", $key = ""){ // to hold select point of dropdown 
    global $singleAssign;
        if(isset($_SESSION["$value"])){
            if($_SESSION["$value"] == "$key"){
                echo 'selected="selected"';
                unset($_SESSION["$value"]);
            }
        }
        elseif($singleAssign["$value"] == "$key"){
             echo 'selected="selected"';
        }
        else{
            echo '';
        }
    }

include_once '../header.php';
include_once 'menubar.php';

$objAssignEdit->session_message('assignUpdateSuccess');
?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel registration-form">
            <div class="panel-body">
                <!-- Content area -->
                <div class="content">
                    <!-- deatched content -->

                    <div class="container-detached">
                        <div class="content-detached">
                            <form class="form-horizontal" action="update_assign.php" method="POST" class="assign">
                                <div class="panel panel-flat">
                                    <div class="panel-body">


<div class="row">
    <div class="col-md-6">
        <fieldset>
            <div class="form-group">
                <label class="col-lg-3 control-label">Course Title:</label>
                <div class="col-lg-9">
                    <select id="firstSelect" name="course_id" data-placeholder="Select Course Title" class="select">
                        <option selected="selected"></option>
                        <optgroup label="Select Course Title">
                            <?php
                            foreach ($allCourses as $oneCourse) {
                            ?>
                            <option value="<?php echo $oneCourse['id']; ?>" <?php session_checked("course_id",$oneCourse['id']); ?>>
                                <?php echo $oneCourse['title']; ?>
                            </option>
                            <?php }
                            ?>
                        </optgroup>
                    </select>
                    <?php errMsg('course_id-required'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Lead Trainer:</label>
                <div class="col-lg-9">
                    <select  name="lead_trainer"  data-placeholder="Select Lead Trainer" class="select">
                        <option selected="selected"></option>
                        <?php
                        foreach ($allTrainers as $oneTrainer) {
                        if ($oneTrainer['trainer_status'] == "lead_trainer") {
                        ?>
                        <option value="<?php
                            echo $oneTrainer['id'];
                            echo '"';
                            session_checked('lead_trainer',$oneTrainer['id']);?> >
                        <?php echo $oneTrainer['full_name'] ?></option>
                        <?php
                        } // if trainer == lead trainer
                        }// foreach
                        ?>
                    </select>
                    <?php errMsg('lead_trainer-required');errMsg('wrongLeadTrainer'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"> Lab Assistant:</label>
                <div class="col-lg-9">
                    <select  name="lab_asst"  data-placeholder="Select Lab Assistant" class="select">
                        <option selected="selected"></option>
                        <?php
                        foreach ($allTrainers as $oneTrainer) {
                        if ($oneTrainer['trainer_status'] == "lab_assist") {
                        ?>
                        <option value="<?php
                            echo $oneTrainer['id'];
                            echo '"';
                            session_checked('lab_asst',$oneTrainer['id']);?> >
                        <?php echo $oneTrainer['full_name'] ?></option>
                        <?php
                        } // if trainer == lab assist
                        }// foreach
                        ?>
                    </select>
                    <?php errMsg('lab_asst-required');errMsg('wrongLabAssist'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Start Date</label>
                <div class="col-lg-9">
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="text" class="form-control" id="startDate" placeholder="Start Date" name="start_date" value="<?php session_DB_value_update('start_date');?>">
                    </div>
                    <?php errMsg('start_date-required');  errMsg('dateErrStr'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Start Time</label>
                <div class="col-lg-9">
                    <div class="input-group bootstrap-timepicker timepicker">
                        <span class="input-group-addon">@</span>
                        <input type="text" class="form-control" placeholder="Start Time" id="startTime" name="start_time" value="<?php session_DB_startTime_endTime('start_time');?>">
                    </div>
                    <?php errMsg('start_time-required');errMsg('timeErrStr'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Day</label>
                <div class="col-lg-9">
                    <select name="day" class="form-control">
                        <option value="day1" <?php session_checked('day','day1');?>>Sat - Mon - Wed</option>
                        <option value="day2" <?php session_checked('day','day2');?>>Sun - Tue - Thus</option>
                        <option value="day3" <?php session_checked('day','day3');?>>Friday</option>
                    </select>
                    <?php
                        errMsg('day-required');
                    ?>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="col-md-6">
        <fieldset>
            <div class="form-group">
                <label class="control-label col-lg-3"> Batch Number</label>
                <div class="col-lg-9">
                    <input type="text" name = "batch_no" class="form-control" placeholder="Batch Number" value="<?php session_DB_value_update('batch_no');?>">
                    <?php errMsg('batch_no-required'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Assist Trainer</label>
                <div class="col-lg-9">
                    <select id="secondSelect" name="asst_trainer"  data-placeholder="Select Assistant Trainer" class="select">
                        <option selected="selected"></option>
                        <?php
                        foreach ($allTrainers as $oneTrainer) {
                        if ($oneTrainer['trainer_status'] == "assist_trainer") {
                        ?>
                        <option class="conditional <?php echo $oneTrainer['courses_id']; ?>" value= "<?php
                            echo $oneTrainer['id'];
                            echo '"';
                            session_checked('asst_trainer',$oneTrainer['id']);?> >
                        <?php echo $oneTrainer['full_name'] ?></option>
                        <?php
                        } // if trainer == lab assist
                        }// foreach
                        ?>
                    </select>
                    <?php errMsg('asst_trainer-required');errMsg('wrongAssistTrainer'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Lab Number</label>
                <div class="col-lg-9">
                    <select data-placeholder="Select Lab Number" class="select" name="lab_id" data-placeholder="Select Lab Number">
                        <option selected="selected"></option>
                        <?php
                        foreach ($allLabs as $oneLabs) {
                        ?>
                        <option value="<?php echo $oneLabs['id']; ?>" <?php session_checked('lab_id',$oneLabs['id']);?>>Lab <?php echo $oneLabs['lab_no'] ?> </option>
                        <?php
                        } // foreach
                        ?>
                    </select>
                    <?php errMsg('lab_id-required'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">End Date</label>
                <div class="col-lg-9">
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="text" class="form-control" placeholder="End Date" name="ending_date" id="endDate" value="<?php session_DB_value_update('ending_date');?>">
                    </div>
                    <?php
                    errMsg('ending_date-required');
                    errMsg('dateErrEnd');
                    errMsg('startDateGreaterThenEndDate');
                    errMsg('startDateEqualEndDate');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">End Time</label>
                <div class="col-lg-9">
                    <div class="input-group bootstrap-timepicker timepicker">
                        <span class="input-group-addon">@</span>
                        <input type="text" class="form-control" placeholder="End Time" id="endTime" name="ending_time" value="<?php session_DB_startTime_endTime('ending_time');?>">
                    </div>
                    <?php
                    errMsg('ending_time-required');
                    errMsg('timeErrEnd');
                    errMsg('startGreaterThenEnd');
                    errMsg('startTimeEqualEndTime');
                    ?>
                </div>
            </div>
            <input type="hidden" name='id' value="<?php echo $singleAssign['id'];?>">
        </fieldset>

    </div>
</div> <?php errMsgSuc('disabled_session'); ?>

                                        <div class="text-right">
                                            <a href="reset.php?resetId=<?php echo $singleAssign['id'];?>" class="btn btn-info">Reset</a>
                                            <button type="submit" class="btn btn-success">Run Course <i class="icon-arrow-right14 position-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <script>
                    $(function () {
                        var conditionalSelect = $("#secondSelect"),
                        // Save possible options
                        options = conditionalSelect.children(".conditional").clone();
                        $("#firstSelect").change(function () {
                        var value = $(this).val();
                            conditionalSelect.children(".conditional").remove();
                            options.clone().filter("." + value).appendTo(conditionalSelect);
                        }).trigger("change");
                    });
                </script>
                <?php
                include_once 'footer.php';
?>