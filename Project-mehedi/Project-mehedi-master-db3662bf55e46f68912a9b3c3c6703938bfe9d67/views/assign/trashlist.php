<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\assign\Assign;
$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objDisableAssign = new Assign();

$allAssignData = $objDisableAssign->disabled_session_list();

$allTrainers = $objDisableAssign->trainer_name_status_team();


        $i = 1; $p = 1;
        $serial = 1;
        include_once '../header.php';
        include_once 'menubar.php';
    ?>
    <!-- Page header -->
<?php $objDisableAssign ->session_message('deleteSuccess') ?>
    <div class="row panel">
        <div class="col-md-12">
           <div class="panel-body">
        <table class="table datatable-basic table-bordered table-striped table-hover dataTable no-footer">
            <thead>
                <tr>
                    <th class="col-md-2"><strong>Course<br>Name</strong></th>
                    <th class="col-md-1"><strong>Batch</strong></th>
                    <th class="col-md-1"><strong>Lead<br>Trainer</strong></th>
                    <th class="col-md-1"><strong>Assist<br>Trainer</strong></th>
                    <th class="col-md-1"><strong>Lab<br>Num</strong></th>
                    <th class="col-md-2 text-center"><strong>Date</strong></th>
                    <th class="col-md-1"><strong>Day<br>Session</strong>/th>
                    <th class="col-md-2 text-center"><strong>Action</strong></th>
                    
                </tr>
            </thead>
            <tbody>
    <?php
    if(isset($allAssignData) && !empty($allAssignData)){
    foreach ($allAssignData as $singleAssign) {

    ?>
    <tr class="<?php
        if ($i % 2 == 0) {
        echo "alpha-indigo";
        } else {
        echo "alpha-slate";
        }
        $i++;
        ?>">
        <td><?php echo $singleAssign['title']; ?></td>
        
        <td><?php echo $singleAssign['batch_no']; ?></td>
        <?php
        foreach($allTrainers as $trainer){
          if($singleAssign['lead_trainer'] == $trainer['id']){
            echo '<td>'.$trainer['full_name'].'</td>';
          }
          if($singleAssign['asst_trainer'] == $trainer['id']){
            echo '<td>'.$trainer['full_name'].'</td>';
          } 
        }
        ?>
        <td><?php echo $singleAssign['lab_no']; ?></td>
        <td class ="text-center">
        <?php echo date('M d Y',strtotime($singleAssign['start_date'])); ?>
         - 
        <?php echo date('M d Y',strtotime($singleAssign['ending_date'])); ?>
        </td>
        <td>
        <?php
            if($singleAssign['day']== 'day1'){
            echo 'Sat-Mon-Wed';}
            if($singleAssign['day']== 'day2'){
            echo 'Sun-Tue-Thu';}
            if($singleAssign['day']== 'day3'){
            echo 'Friday';}
        ?></td>
        <td class="text-center">
            <a class="btn border-teal-800 text-teal-800 btn-flat btn-xs" type="button" href="single_session.php?id=<?php echo $singleAssign['id']; ?>">
                <i class="icon-enlarge6"></i>
            </a>
            <a class="btn border-teal-800 text-teal-800 btn-flat btn-icon btn-xs" type="button" href="restore.php?id=<?php echo $singleAssign['id']; ?>">
                <i class="icon-reset"></i>
            </a>
            <?php
                if($_SESSION['logged']['is_admin'] == 1){
            ?>
            <a class="btn border-teal-800 text-teal-800 btn-flat btn-icon btn-xs" type="button" href="delete.php?id=<?php echo $singleAssign['id']; ?>" onclick="return confirm('Are you sure you want to parmently delete this  session?');"><i class="icon-trash"></i>
            </a>
            <?php
            }
            ?>
        </td>

    </tr>
        <?php
            }// foreach
        }// if $allLabInfo not empty
        ?>
            </tbody>
        </table>
       
        </div>
    </div>
    <?php include_once 'footer.php';?>