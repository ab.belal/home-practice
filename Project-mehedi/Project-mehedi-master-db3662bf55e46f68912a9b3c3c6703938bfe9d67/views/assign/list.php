<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\assign\Assign;
$objLoginUser = new user_login();
$objLoginUser -> login_check();
$objAllAssign = new Assign();

$allAssignData = $objAllAssign->all_assign_with_other_table();

$allTrainers = $objAllAssign->trainer_name_status_team();


        $i = 1; $p = 1;
        $serial = 1;
        include_once '../header.php';
        include_once 'menubar.php';
    ?>
    <!-- Page header -->
<?php $objAllAssign ->session_message('Success') ?>
    <div class="row panel">
        <div class="col-md-12">
           <div class="panel-body">
        <table class="table datatable-basic table-bordered table-striped table-hover dataTable no-footer">
            <thead>
                <tr>
                    <th class="col-md-1"><strong>Course<br>Name</strong></th>
                    <th class="col-md-1"><strong>Batch</strong></th>
                    <th class="col-md-1"><strong>Lead<br>Trainer</strong></th>
                    <th class="col-md-1"><strong>Assist<br>Trainer</strong></th>
                    <th class="col-md-1"><strong>Lab<br>Num</strong></th>
                    <th class="col-md-3 text-center"><strong>Date</strong></th>
                    <th class="col-md-1"><strong>Day</strong></th>
                    <th class="col-md-2 text-center"><strong>Action</strong></th>
                    
                </tr>
            </thead>
            <tbody>
    <?php
    if(isset($allAssignData) && !empty($allAssignData)){
    foreach ($allAssignData as $singleAssign) {

    ?>
    <tr class="<?php
        if ($i % 2 == 0) {
        echo "alpha-indigo";
        } else {
        echo "alpha-slate";
        }
        $i++;
        ?>">
        <td><?php echo $singleAssign['title']; ?></td>
        
        <td><?php echo $singleAssign['batch_no']; ?></td>
        <?php
        foreach($allTrainers as $trainer){
          if($singleAssign['lead_trainer'] == $trainer['id']){
            echo '<td>'.$trainer['full_name'].'</td>';
          }
          if($singleAssign['asst_trainer'] == $trainer['id']){
            echo '<td>'.$trainer['full_name'].'</td>';
          } 
        }
        ?>
        <td><?php echo $singleAssign['lab_no']; ?></td>
        <td class ="text-size-small text-center" style="padding:5px;">
        <?php echo date('M d Y',strtotime($singleAssign['start_date'])); ?>
         - 
        <?php echo date('M d Y',strtotime($singleAssign['ending_date'])); ?>
        <br>
        <?php echo date('h:i A',strtotime($singleAssign["start_time"])).'-'.date('h:i A',strtotime($singleAssign["ending_time"])); ?>

        </td>
        <td>
        <?php
            if($singleAssign['day']== 'day1'){
            echo 'Sat-Mon-Wed';}
            if($singleAssign['day']== 'day2'){
            echo 'Sun-Tue-Thu';}
            if($singleAssign['day']== 'day3'){
            echo 'Friday';}
        ?></td>
        <td class="text-center">
            <a class="btn bg-teal-800 btn-icon btn-xs" type="button" href="single_session.php?id=<?php echo $singleAssign['id']; ?>">
                <i class="icon-enlarge6"></i>
            </a>
            <a class="btn bg-teal-800 btn-icon btn-xs" type="button" href="edit_assign.php?id=<?php echo $singleAssign['id']; ?>">
                <i class="icon-pencil7"></i>
            </a>
            <?php
                if($_SESSION['logged']['is_admin'] == 1){
            ?>
            <a class="btn bg-teal-800 btn-icon btn-xs" type="button" href="trash.php?id=<?php echo $singleAssign['id']; ?>" onclick="return confirm('Are you sure you want to disable this course session?');"><i class="icon-close2"></i>
            </a>
            <?php
            }
            ?>
        </td>

    </tr>
        <?php
            }// foreach
        }// if $allLabInfo not empty
        ?>
            </tbody>
        </table>
       
        </div>
    </div>
    <?php include_once 'footer.php';?>