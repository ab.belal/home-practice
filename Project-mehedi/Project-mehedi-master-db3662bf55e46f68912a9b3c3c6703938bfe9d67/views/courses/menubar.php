                        <ul class="breadcrumb-elements">
                            <li><a href="../courses/add_course.php"><i class=" icon-pen-plus position-left text-teal-800"></i> Add New Course</a></li>

                            <li><a href="../courses/index.php"><i class="icon-graduation2 position-left text-teal-800"></i> View All Course</a></li>

                            <li><a href="../courses/disabled_course.php"><i class="icon-cancel-square position-left text-teal-800"></i> Disabled Course</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">