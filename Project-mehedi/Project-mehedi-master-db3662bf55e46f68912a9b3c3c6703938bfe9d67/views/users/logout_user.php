<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_registration\user_registration;

use ProjectMehedi\user\user_login\user_login;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objEditUser = new user_registration();

session_unset($_SESSION['logged']);
session_destroy();
header("location:../../index.php");

