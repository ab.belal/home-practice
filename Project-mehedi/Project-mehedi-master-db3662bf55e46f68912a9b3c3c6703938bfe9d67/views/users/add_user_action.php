<?php

require_once '../../vendor/autoload.php';

use ProjectMehedi\user\user_registration\user_registration;

$objAddUser = new user_registration();

use ProjectMehedi\user\user_login\user_login;

$objLoginUser = new user_login();
$objLoginUser -> login_check();



$objAddUser -> prepare($_POST);
$objAddUser -> validate();
$objAddUser -> add_image();
$objAddUser -> addUser();

?>