<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_registration\user_registration;

use ProjectMehedi\user\user_login\user_login;

$objLoginUser = new user_login();

$objLoginUser -> login_check();

$objUpdateUser = new user_registration();



if(empty($_POST['password']) && empty($_POST['confrm_password'])){
	$_POST['password'] = $_SESSION['pass'];
	$_POST['confrm_password'] = $_SESSION['pass'];
}

if(empty($_POST['image'])){
	$_POST['image'] = $_SESSION['img'];
}

$objUpdateUser -> prepare($_POST);

// echo "<pre>";
// print_r($objUpdateUser -> prepare($_POST));


$objUpdateUser -> validate();

$objUpdateUser -> add_image();

$objUpdateUser -> update_user();