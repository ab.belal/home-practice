<?php
require_once '../../vendor/autoload.php';
use ProjectMehedi\user\user_login\user_login;
use ProjectMehedi\software\software;

$objLoginUser = new user_login();
$objLoginUser -> login_check();

$objAddsoftware = new software();

$labinfo = $objAddsoftware -> lab_id_labno();

    function errMsg($data = ""){ // Show the Error session msg from class file
        if(isset($_SESSION["$data"])){
        ?>
            <label class="validation-error-label text-left">
        <?php
            if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
                echo $_SESSION["$data"]."<br></label>";
                unset($_SESSION["$data"]);
            }
        }
    }

    function errMsgSuc($data = ""){ // Show the Error session msg in Green color
        if(isset($_SESSION["$data"])){
        ?>
            <label class="validation-error-label text-success text-left">
        <?php
            if (!empty($_SESSION["$data"]) && isset($_SESSION["$data"])) {
                echo $_SESSION["$data"]."<br></label>";
                unset($_SESSION["$data"]);
            }
        }
    }

    function session_value($data = ""){ // show the session msg in value prevent blank field.
        if(isset($_SESSION["$data"])){
            echo $_SESSION["$data"];
        }
    }

    function session_checked($value = "", $key = ""){ // to hold select point of dropdown 
        if(isset($_SESSION["$value"])){
            if($_SESSION["$value"] == "$key"){
                echo 'selected="selected"';
            }else{
                echo '';
            }
        }
    }

    function session_checked_radio($value = "", $key = ""){ // to hold checked of Radio 
        if(isset($_SESSION["$value"])){
            if($_SESSION["$value"] == "$key"){
                echo 'checked="checked"';
            }else{
                echo '';
            }
        }
    }


include_once '../header.php';
include_once 'menubar.php';
?>
<!-- Table -->
<?php $objAddsoftware -> session_message('labAddSuccess'); ?>
<div class="panel-flat">
    <!-- Grid -->
    
    <div class="row">
        <div class="col-lg-8 col-md-offset-2">
            <div class="panel registration-form">
                <div class="panel-body">
                    <div class="text-center">
                        <div class="icon-object border-info text-info"><i class="icon-gear"></i>
                        </div>
                        <h5 class="content-group-lg">Add New Software</h5>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Horizontal form -->
                            <div class="panel panel-flat">
                                <div class="panel-body text-center">

                                <form action="add_software_action.php" class="form-horizontal software" method="POST">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Installed in Lab</label>
                                                <div class="col-lg-9">
                                                    <select class="bootstrap-select" data-width="100%" name="labinfo_id">
                                                        <option selected="selected"></option>

                                            <?php
                                                foreach($labinfo as $lab){
                                            ?>

                                            <option value="<?php echo $lab['id'];?>" <?php session_checked('labinfo_id',$lab['id']);?>>Lab Number <?php echo $lab['lab_no'];?>
                                            </option>
                                            <?php
                                                }
                                            ?>
                                                    </select>
                                                    <?php errMsg('labinfo_id-required');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Software Title</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" placeholder="Provide Software Title" name="software_title" value="<?php session_value('software_title');?>">
                                                    <?php errMsg('software_title-required');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Software Version</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" placeholder="Provide Software Version" name="version" value="<?php session_value('version');?>">
                                                    <?php errMsgSuc('version-required');?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-lg-3">Software Type</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" placeholder="Provide Software Type" name="software_type" value="<?php session_value('software_type');?>">
                                                    <?php errMsg('software_type-required');?>
                                                </div>
                                            </div>
                                            <div class="text-right">
                                            <button class="btn bg-info" type="submit">Add Software<i class="icon-arrow-right14 position-right"></i></button>
                                        </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <!-- /grid -->
    
    <script type="text/javascript" src="../assets/js/pages/uploader_bootstrap.js"></script>

    <script type="text/javascript" src="../assets/js/plugins/uploaders/fileinput.min.js"></script>

    <?php include_once 'footer.php' ?>